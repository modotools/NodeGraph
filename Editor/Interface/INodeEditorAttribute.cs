﻿using System;

namespace nodegraph.Editor.Interface
{
    public interface INodeEditorAttribute
    {
        Type GetInspectedType();
    }
}
