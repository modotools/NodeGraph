﻿using System;
using UnityEditor;
using UnityEngine;
using nodegraph.Attributes;

namespace nodegraph.Editor.Drawers
{
    [CustomPropertyDrawer(typeof(NodeEnumAttribute))]
    public class NodeEnumDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            EnumPopup(position, property, label);

            EditorGUI.EndProperty();
        }

        static void EnumPopup(Rect position, SerializedProperty property, GUIContent label)
        {
            // Throw error on wrong type
            if (property.propertyType != SerializedPropertyType.Enum)
                throw new ArgumentException("Parameter selected must be of type System.Enum");

            // Add label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Get current enum name
            var enumName = "";
            if (property.enumValueIndex >= 0 && property.enumValueIndex < property.enumDisplayNames.Length) enumName = property.enumDisplayNames[property.enumValueIndex];

            // Display dropdown
            if (EditorGUI.DropdownButton(position, new GUIContent(enumName), FocusType.Passive))
            {
                // Position is all wrong if we show the dropdown during the node draw phase.
                // Instead, add it to onLateGUI to display it later.
                NodeEditorWindow.Current.OnLateGUI += () => ShowContextMenuAtMouse(property);
            }
        }

        static void ShowContextMenuAtMouse(SerializedProperty property)
        {
            // Initialize menu
            var menu = new GenericMenu();

            // Add all enum display names to menu
            for (var i = 0; i < property.enumDisplayNames.Length; i++)
            {
                var index = i;
                menu.AddItem(new GUIContent(property.enumDisplayNames[i]), false, () => SetEnum(property, index));
            }

            // Display at cursor position
            var r = new Rect(Event.current.mousePosition, new Vector2(0, 0));
            menu.DropDown(r);
        }

        static void SetEnum(SerializedProperty property, int index)
        {
            property.enumValueIndex = index;
            property.serializedObject.ApplyModifiedProperties();
            property.serializedObject.Update();
        }
    }
}