﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Types;
using UnityEditor;
using UnityEngine;
using nodegraph.Data;
using nodegraph.Operations;
using Object = UnityEngine.Object;

namespace nodegraph.Editor.Operations
{
    public static class NodePortOperations
    {
        /// <summary> Connect one <see cref="NodePort"/> to another </summary>
        /// <param name="from">The <see cref="NodePort"/> to connect from</param>
        /// <param name="to">The <see cref="NodePort"/> to connect to</param>
        public static void Connect(NodePort from, NodePort to)
        {
            var canConnect = from.CanConnect(to) && to.CanConnect(from);
            if (!canConnect)
                return;
            FixDirection(ref @from, ref to);

            var graph = from.Node.Graph;
            var graphObj = (ScriptableObject)graph;
            Undo.RecordObjects(new Object[] { (Object) @from.Node, (Object) to.Node, graphObj }, "Connect Port");

            if (from.ConnectionType == ConnectionType.Override && from.ConnectionCount != 0)
                from.ClearConnections();
            if (to.ConnectionType == ConnectionType.Override && to.ConnectionCount != 0)
                to.ClearConnections();

            //Debug.Log($"New Connection made! {from.ID} to {to.ID}");
            graph.AddConnection(new Connection() { From = from.ID, To = to.ID });

            from.Node.OnCreateConnection(from.ID);
            to.Node.OnCreateConnection(to.ID);
        }

        public static void FixDirection(ref NodePort @from, ref NodePort to)
        {
            if (@from.Direction == IO.Output)
                return;

            var temp = @from;
            @from = to;
            to = temp;
        }

        public static bool CanConnect(this NodePort @this, NodePort port)
        {
            if (port == null)
            {
                Debug.LogWarning("Cannot connect to null port");
                return false;
            }

            if (port == @this)
            {
                Debug.LogWarning("Cannot connect port to self.");
                return false;
            }

            if (@this.IsConnectedTo(port))
            {
                Debug.LogWarning("Port already connected. ");
                return false;
            }

            if (@this.Direction == port.Direction)
            {
                Debug.LogWarning("Cannot connect two " + (@this.Direction == IO.Input ? "input" : "output") + " connections");
                return false;
            }

            return true;
        }

        public static List<NodePort> GetConnections(this NodePort @this) => @this.Connections
            .Select(@this.GetConnectedPort)
            .Where(port => port != null).ToList();

        public static PortID GetConnectedPortID(this NodePort @this, Connection c) =>
            c.From == @this.ID
                ? c.To
                : c.To == @this.ID
                    ? c.From
                    : default;

        public static NodePort GetConnectedPort(this NodePort @this, Connection c) => @this.Graph.GetPort(@this.GetConnectedPortID(c));

        public static bool IsConnectedTo(this NodePort @this, NodePort port)
            => @this.Connections.Any(c => c.From == port.ID || c.To == port.ID);

        /// <summary> Returns true if this port can connect to specified port </summary>
        public static bool CanConnectTo(this NodePort @this, NodePort port)
        {
            // Figure out which is input and which is output
            NodePort input = null, output = null;
            if (@this.IsInput)
                input = @this;
            else
                output = @this;
            if (port.IsInput)
                input = port;
            else
                output = port;
            // If there isn't one of each, they can't connect
            if (input == null || output == null)
                return false;

            switch (input.TypeConstraint)
            {
                // Check input type constraints
                case TypeConstraint.Inherited when !input.ValueType.IsAssignableFrom(output.ValueType):
                case TypeConstraint.Strict when input.ValueType != output.ValueType:
                case TypeConstraint.InheritedInverse when !output.ValueType.IsAssignableFrom(input.ValueType):
                case TypeConstraint.InheritedAny when !input.ValueType.IsAssignableFrom(output.ValueType) && !output.ValueType.IsAssignableFrom(input.ValueType):
                    return false;
                case TypeConstraint.None: break;
                default: throw new ArgumentOutOfRangeException();
            }
            switch (output.TypeConstraint)
            {
                // Check output type constraints
                case TypeConstraint.Inherited when !input.ValueType.IsAssignableFrom(output.ValueType):
                case TypeConstraint.Strict when input.ValueType != output.ValueType:
                case TypeConstraint.InheritedInverse when !output.ValueType.IsAssignableFrom(input.ValueType):
                case TypeConstraint.InheritedAny when !input.ValueType.IsAssignableFrom(output.ValueType) && !output.ValueType.IsAssignableFrom(input.ValueType):
                    return false;
                case TypeConstraint.None:
                default: // Success
                    return true;
            }
        }

        /// <summary> Disconnect two ports </summary>
        public static void Disconnect(NodePort a, NodePort b)
        {
            if (a == null || b == null)
                return;

            a.Graph.Disconnect(a.ID, b.ID);

            EditorUtility.SetDirty((Object) a.Node);
            EditorUtility.SetDirty((Object) b.Node);

            // Trigger OnRemoveConnection
            a.Node.OnRemoveConnection(a.ID);
            b.Node.OnRemoveConnection(b.ID);
        }

        public static void ClearConnections(this NodePort @this)
        {
            //if (IsConnected) Debug.Log($"Clear Connections {Connections.Count()}");
            while (@this.IsConnected)
            {
                var otherId = @this.GetConnectedPortID(@this.Connections.FirstOrDefault());
                @this. Graph.Disconnect(@this.ID, otherId);
            }
        }

        /// <summary> Swap connections with another node </summary>
        public static void SwapConnections(NodePort a, NodePort b)
        {
            //Debug.Log($"{nameof(SwapConnections)}");

            // Cache port connections
            var aConnectionPorts = a.Connections.Select(a.GetConnectedPort).ToList();
            var bConnectionPorts = b.Connections.Select(b.GetConnectedPort).ToList();

            a.ClearConnections();
            b.ClearConnections();
            // Add port connections to targetPort
            foreach (var acp in aConnectionPorts)
                Connect(b, acp);
            // Add target port connections to this one
            foreach (var bcp in bConnectionPorts)
                Connect(a, bcp);
        }

        /// <summary> Copy all connections pointing to a node and add them to this one </summary>
        public static void AddConnections(this NodePort @this, NodePort targetPort)
        {
            //Debug.Log($"{nameof(AddConnections)}");
            foreach (var c in targetPort.Connections)
            {
                var otherPort = targetPort.GetConnectedPort(c);
                Connect(@this, otherPort);
            }
        }

        /// <summary> Swap connected nodes from the old list with nodes from the new list </summary>
        public static void Redirect(this NodePort @this, IList<INode> oldNodes, IReadOnlyList<INode> newNodes)
        {
            using (var oldConnections = SimplePool<List<Connection>>.I.GetScoped())
            using (var newConnections = SimplePool<List<Connection>>.I.GetScoped())
            {
                foreach (var c in @this.Connections)
                {
                    var otherNode = @this.GetConnectedPort(c).Node;
                    var index = oldNodes.IndexOf(otherNode);
                    if (index < 0)
                        continue;

                    oldConnections.Obj.Add(c);
                    var newCon = c;
                    @this.SetOtherNode(ref newCon, (ScriptableObject)newNodes[index]);
                    newConnections.Obj.Add(newCon);
                }
            }

        }

        static void SetOtherNode(this NodePort @this, ref Connection newCon, ScriptableObject node)
        {
            if (newCon.From == @this.ID)
                newCon.To.Node = node;
            else
                newCon.From.Node = node;
        }


        public static bool HasOutwardConnection(this NodePort port, IEnumerable<INode> selectedNodes) 
            => port.Connections.Any(c => port.ConnectsOutward(c, selectedNodes));

        static bool ConnectsOutward(this NodePort port, Connection c, IEnumerable<INode> selectedNodes)
        {
            if (!(port.GetConnectedPortID(c).Node is INode node))
                return false;
            return !selectedNodes.Contains(node);
        }
    }
}
