﻿using System.Linq;
using Core.Editor.Data;
using Core.Editor.Utility;
using Core.Events;
using Core.Interface;
using Core.Runtime.Interface;
using UnityEditor;
using UnityEngine;
using nodegraph;
using nodegraph.Editor;
using nodegraph.Editor.Data;
using nodegraph.Editor.Interface;
using nodegraph.Editor.Resources;
using nodegraph.Editor.Settings;
using nodegraph.Editor.Utility;
using nodegraph.Operations;

using static nodegraph.Editor.Operations.NodeOperations;
using static Core.Editor.Extensions.GenericMenuExtension;
public static class NodeEditorOperations
{
    static readonly Color k_defaultColor = new Color32(90, 97, 105, 255);

    public static void DefaultHeader(this INodeEditor @this)
        => GUILayout.Label(@this.Target.Name, @this.GetStyles().NodeHeader, GUILayout.Height(30));

    public static void DefaultBodyGUI<T>(this INodeEditor @this, SerializedObject dataObj, T target)
        where T : INode
    {
        // Unity specifically requires this to save/update any serial object.
        // serializedObject.Update(); must go at the start of an inspector gui, and
        // serializedObject.ApplyModifiedProperties(); goes at the end.
        dataObj.Update();
        var excludes = target.Editor_GUIExcludes;
        //{ "m_Script", nameof(Node.Graph), nameof(Node.Position), Node.PortsPropName };

        // Iterate through serialized properties and draw them like the Inspector (But with ports)
        var iterator = dataObj.GetIterator();
        var visibleIterator = dataObj.GetIterator();
        var enterChildren = true;
        var onlyPorts = NodeEditorPreferences.GetSettings().DrawOnlyPorts;

        visibleIterator.NextVisible(true);
        while (iterator.Next(enterChildren))
        {
            var visible = (string.Equals(visibleIterator.propertyPath, iterator.propertyPath));
            if (visible)
                visibleIterator.NextVisible(false);
            var drawOnlyWithPort = !visible || onlyPorts;
            //Debug.Log($"{iterator.propertyPath} {visible}");
            enterChildren = false;
            if (excludes.Contains(iterator.name))
                continue;
            NodeEditorGUILayout.PropertyField(iterator, drawOnlyWithPort: drawOnlyWithPort); //, includeChildren: true);
        }

        // Iterate through dynamic ports and draw them in the order in which they are serialized
        foreach (var dynamicPort in target.DynamicPorts())
        {
            if (NodeEditorGUILayout.IsDynamicPortListPort(dynamicPort))
                continue;
            NodeEditorGUILayout.PortField(dynamicPort);
        }

        dataObj.ApplyModifiedProperties();
    }

    public static int DefaultGetWidth(this INodeEditor @this, INodeData data)
    {
        var type = data.GetType();
        return type.TryGetAttributeWidth(out var width) ? width : NodeOperations.DefaultNodeSize;
    }
    public static Color DefaultGetTint(this INodeEditor @this, INodeData data)
        => @this.DefaultGetTint(data, k_defaultColor);
    public static Color DefaultGetTint(this INodeEditor @this, INodeData data, Color fallBack)
    {
        if (data is IGUIColor tint)
            return tint.Color;
        // Try get color from [NodeTint] attribute
        var type = data.GetType();
        return type.TryGetAttributeTint(out var color)
            ? color
            : k_defaultColor;
    }
    public static NodeStyles DefaultGetStyles(this INodeEditor @this) => NodeEditorResources.Styles;

    public static void DefaultAddContextMenuItems(this INodeEditor @this, GenericMenu menu)
    {
        var canRemove = true;

        var selectedNode = (INode)Selection.activeObject;
        var isNode = selectedNode != null;
        var isNameable = selectedNode is INameable;

        // Actions if only one node is selected
        if (Selection.objects.Length == 1 && isNode)
        {
            menu.AddItem(new GUIContent("Move To Top"), false, () => NodeEditorWindow.Current.MoveNodeToTop(selectedNode));
            menu.AddItem(new GUIContent("Rename"), false, NodeEditorWindow.Current.RenameSelectedNode, isNameable);

            var graphEditor = NodeEditorCache.GetEditor(selectedNode.Graph, NodeEditorWindow.Current);
            canRemove = graphEditor.CanRemove(selectedNode);
        }

        // Add actions to any number of selected nodes
        menu.AddItem(new GUIContent("Copy"), false, NodeEditorWindow.Current.CopySelectedNodes);
        menu.AddItem(new GUIContent("Duplicate"), false, NodeEditorWindow.Current.DuplicateSelectedNodes);

        menu.AddItem(new GUIContent("Remove"), false, NodeEditorWindow.Current.RemoveSelectedNodes, canRemove);

        // Custom sections if only one node is selected
        if (Selection.objects.Length != 1 || !isNode)
            return;

        menu.AddItem(new GUIContent("Clear Dynamic Ports"), false, selectedNode.ClearDynamicPorts);

        menu.AddCustomContextMenuItems(selectedNode);
    }

    public static void DefaultRename(this INodeEditor @this, string newName)
    {
        var prevName = @this.TargetObj.name;
        if (newName == null || newName.Trim() == "")
            newName = @this.TargetObj.DefaultName();

        @this.TargetObj.name = newName;

        EventMessenger.TriggerEvent(new AssetRenamed()
        {
            OldName = prevName,
            NewName = newName,
            RenamedAsset = @this.TargetObj
        });
        
        if (@this.TargetObj is IRenamedHandler r)
            r.OnRenamed();

        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(@this.TargetObj));
    }
}
