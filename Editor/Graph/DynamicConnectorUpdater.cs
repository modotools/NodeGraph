﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Events;
using Core.Extensions;
using Core.Types;
using UnityEditor;
using UnityEngine;
using nodegraph.Attributes;
using nodegraph.Editor.Data;
using nodegraph.Editor.Interface;
using nodegraph.Editor.Operations;
using Object = UnityEngine.Object;

namespace nodegraph.Editor.Graph
{ 
    public class DynamicConnectorUpdater : Singleton<DynamicConnectorUpdater>, IEventListener<ObjectChanged>, IEventListener<NodeUpdated>
    {
        // todo!!:
        const string k_selfPort = "this";

        #region Node Management
        readonly Dictionary<INode, NodeConnectorMemory> m_nodes = new Dictionary<INode, NodeConnectorMemory>();
        NodeConnectorMemory GetOrAddDataForNode(INodeEditor editor, INode targetNode)
        {
            if (m_nodes.TryGetValue(targetNode, out var mem)) 
                return mem;
            mem = NodeConnectorMemory.Default;
            mem.Editor = editor;
            m_nodes[targetNode] = mem;

            return mem;
        }
        #endregion

        #region Init Dynamic Ports
        public void InitDynamicPorts(INodeEditor nodeEditor)
        {
            if (!(nodeEditor.TargetObj is INode node))
                return;

            if (m_nodes.TryGetValue(node, out var mem)) 
                ClearMemory(mem);

            var it = nodeEditor.SerializedObjectData.GetIterator();
            var enterChildren = true;
            while (it.Next(enterChildren))
            {
                enterChildren = false;

                InitPortForProperty(nodeEditor, it, node);
            }
        }

        void InitPortForProperty(INodeEditor nodeEditor, SerializedProperty it, INode node)
        {
            if (TryInitListMulti(nodeEditor, it, node)) 
                return;

            if (TryInitList(nodeEditor, it, node)) 
                return;

            TryInitSingle(nodeEditor, it, node);
        }

        void TryInitSingle(INodeEditor nodeEditor, SerializedProperty it, INode node)
        {
            var singleAttr = it.GetAttribute<NodeConnectorAttribute>();
            if (singleAttr == null) 
                return;

            var propData = new ConnectorPropData(it.propertyPath, singleAttr.Data, false);
            UpdateDynamicConnector(nodeEditor, node, it, propData);
        }

        bool TryInitListMulti(INodeEditor nodeEditor, SerializedProperty it, INode node)
        {
            var listAttr = it.GetAttribute<NodeConnectorListMultiAttribute>();
            if (!it.isArray || listAttr == null) 
                return false;

            var pathToMulti = it.propertyPath;

            foreach (var relPath in listAttr.RelativePaths)
            {
                var fi = listAttr.DataType.GetField(relPath);
                var attribute = fi.GetCustomAttributes(typeof(NodeConnectorAttribute), true)
                    .OfType<NodeConnectorAttribute>().FirstOrDefault();
                if (attribute == null)
                    continue;
                //Debug.Log("singleAttr in multi found");

                var propData = new ConnectorPropData(pathToMulti, attribute.Data, true);
                UpdateDynamicConnectorLists(nodeEditor, node, it, propData);
            }

            return true;
        }


        bool TryInitList(INodeEditor nodeEditor, SerializedProperty it, INode node)
        {
            var listAttr = it.GetAttribute<NodeConnectorListAttribute>();
            if (!it.isArray || listAttr == null) 
                return false;

            var propData = new ConnectorPropData(it.propertyPath, listAttr.Data, true);
            UpdateDynamicConnectorLists(nodeEditor, node, it, propData);
            return true;
        }

        static void ClearMemory(NodeConnectorMemory mem)
        {
            mem.Memory.Clear();
            mem.ListMemory.Clear();
        }
        #endregion

        void UpdateDynamicConnectorLists(INodeEditor nodeEditor, INode targetNode, SerializedProperty it, ConnectorPropData propData, 
            ConnectorPort[] previous = null)
        {
            //Debug.Log($"{nameof(UpdateDynamicConnectorLists)} {it.propertyPath}");
            DataListToMemoryStruct(it, propData, out var memory);
            if (propData.SyncData)
                ConnectGraph(targetNode.Graph, memory);

            var size = memory.Ports.Length;
            //var fieldName = it.propertyPath;

            var mem = GetOrAddDataForNode(nodeEditor, targetNode);
            mem.ListMemory.Add(memory);

            var mandatoryPorts = new NodePort[size];
            using (var removePorts = SimplePool<List<NodePort>>.I.GetScoped())
            {
                CollectExistingPorts(targetNode, propData.ConnectorType, mandatoryPorts, memory, previous, removePorts.Obj);
                AddMissingPorts(targetNode, propData.ConnectorType, memory, mandatoryPorts);
                UpdateConnectionsFromMemory(mandatoryPorts, memory);
                RemovePorts(targetNode, removePorts.Obj);
            }
            //Debug.Log($"size {size} outputs {outputs.Count()} propPath {it.propertyPath} name {it.name}");
        }

        void UpdateDynamicConnector(INodeEditor nodeEditor, INode targetNode, SerializedProperty it, ConnectorPropData propData, ConnectorPort previous = default)
        {
            //Debug.Log($"{nameof(UpdateDynamicConnector)} {it.propertyPath}");

            DataToMemoryStruct(it, propData, out var memory);
            if (propData.SyncData)
                ConnectGraph(targetNode.Graph, memory.Port.Data);

            var mem = GetOrAddDataForNode(nodeEditor, targetNode);
            mem.Memory.Add(memory);

            var output = GetOrCreatePort(targetNode, propData, memory.Port.FieldName, previous);
            if (propData.SyncData)
                UpdateConnection(memory.Port.Data, output);
        }

        #region UpdateDynamic Methods
        static void UpdateConnectionsFromMemory(IReadOnlyList<NodePort> mandatoryPorts, ConnectorListMemory memory)
        {
            if (!memory.PropData.SyncData)
                return;

            for (var i = 0; i < mandatoryPorts.Count; i++)
            {
                var from = mandatoryPorts[i];
                if (@from == null)
                {
                    Debug.LogError($"Unexpected missing dynamic port at {i}");
                    return;
                }
                UpdateConnection(memory.Ports[i].Data, @from);
            }
        }

        static void UpdateConnection(INodeData data, NodePort @from)
        {
            var needConnection = data != null;
            var connectedTo = (INode)@from.ConnectedToPortID.Node;

            if (connectedTo?.NodeData == data)
                return;

            if (!needConnection)
            {
                //Debug.Log($"Cleared Connections for {@from.FieldName}");
                @from.ClearConnections();
                return;
            }

            //Debug.Log($"Connected {@from.FieldName} to {data.name}");
            var to = data.Node.GetInputPort(k_selfPort);
            NodePortOperations.Connect(@from, to);
        }

        static void RemovePorts(INode node, IEnumerable<NodePort> removePorts)
        {
            //Debug.Log($"Removing Port {p.FieldName}");
            foreach (var p in removePorts) 
                node.RemoveDynamicPort(p);
        }

        static void AddMissingPorts(INode node, Type connectorType, ConnectorListMemory memory, IList<NodePort> mandatoryPorts)
        {
            for (var i = 0; i < mandatoryPorts.Count; i++)
            {
                if (mandatoryPorts[i] != null)
                    continue;

                var outputName = memory.Ports[i].FieldName;
                //Debug.Log($"Added Dynamic Port {outputName}");

                mandatoryPorts[i] = memory.PropData.IO == IO.Output
                    ? node.AddDynamicOutput(connectorType, ConnectionType.Override, fieldName: outputName)
                    : node.AddDynamicInput(connectorType, ConnectionType.Override, fieldName: outputName);
            }
        }

        static void CollectExistingPorts(INode node, Type connectorType, IList<NodePort> mandatoryPorts, ConnectorListMemory memory, 
            ConnectorPort[] previous, ICollection<NodePort> removePorts)
        {
            var dynamicPorts = memory.PropData.IO == IO.Output ? node.DynamicOutputs() : node.DynamicInputs();
            var connectingPorts = dynamicPorts.Where(port => port.ValueType == connectorType);

            foreach (var o in connectingPorts)
            {
                var outputName = o.FieldName;
                var idx = Array.FindIndex(memory.Ports, p => string.Equals(outputName, p.FieldName));
                if (idx == -1)
                {
                    if (previous == null)
                        continue;

                    var prevIdx = Array.FindIndex(previous, p => string.Equals(outputName, p.FieldName));
                    if (prevIdx != -1) 
                        removePorts.Add(o);

                    continue;
                }

                mandatoryPorts[idx] = o;
            }
        }

        #region Data Conversion
        static void DataListToMemoryStruct(SerializedProperty it, ConnectorPropData propData, out ConnectorListMemory memory)
        {
            var size = it.arraySize;
            var arr = new ConnectorPort[size];
            for (var i = 0; i < size; i++)
            {
                var elProp = it.GetArrayElementAtIndex(i);
                propData.GetNameAndObjProp(elProp, i, 
                    out var fieldName, out var objProp);
                //Debug.Log($"{fieldName}");
                var port = GetPortData(propData, objProp, fieldName);
                arr[i] = port;
            }
            memory = new ConnectorListMemory()
            {
                PropData = propData,
                Ports = arr,
            };
        }

        static void DataToMemoryStruct(SerializedProperty it, ConnectorPropData propData, out ConnectorMemory memory)
        {
            propData.GetNameAndObjProp(it, out var fieldName, out var objProp);
            var port = GetPortData(propData, objProp, fieldName); 

            memory = new ConnectorMemory()
            {
                PropData = propData,
                Port = port,
            };
        }

        static ConnectorPort GetPortData(ConnectorPropData propData, SerializedProperty objectProp, string fieldName)
        {
            ConnectorPort port = default;
            port.FieldName = fieldName;

            if (objectProp.propertyType == SerializedPropertyType.ObjectReference && propData.SyncData)
                port.Data = objectProp.objectReferenceValue as INodeData;

            return port;
        }
        #endregion

        static void ConnectGraph(INodeGraph graph, ConnectorListMemory memory)
        {
            foreach (var data in memory.Ports)
                ConnectGraph(graph, data.Data);
        }

        static void ConnectGraph(INodeGraph graph, INodeData data)
        {
            if (data == null)
                return;
            var node = data.Node;

            if (node == null)
            {
                var wrapper = ScriptableObject.CreateInstance<WrapperNode>();
                wrapper.Editor_SetNodeData(data);
                ((IOptionalNode)data).SetNode(wrapper);
                node = wrapper;
            }

            if (graph.Contains(node))
                return;

            // todo: add sub-graph-node instead
            graph.Add(node);
            node.Graph = graph;
        }

        static NodePort GetOrCreatePort(INode targetNode, ConnectorPropData propData, string fieldName, 
            ConnectorPort previous = default)
        {
            var dynamicPorts = propData.IO == IO.Output ? targetNode.DynamicOutputs() : targetNode.DynamicInputs();
            var connectingPorts = dynamicPorts.Where(port => port.ValueType == propData.ConnectorType);

            NodePort prev = null;
            NodePort dynamicPort = null;

            foreach (var o in connectingPorts)
            {
                var outputName = o.FieldName;

                if (string.Equals(fieldName, outputName))
                    dynamicPort = o;
                else if (string.Equals(previous.FieldName, outputName)) 
                    prev = o;
            }

            if (prev != null) 
                targetNode.RemoveDynamicPort(prev);
            if (dynamicPort != null)
                return dynamicPort;

            //Debug.Log($"Added Dynamic Port {fieldName}");
            dynamicPort = propData.IO == IO.Output 
                ? targetNode.AddDynamicOutput(propData.ConnectorType, ConnectionType.Override, fieldName: fieldName) 
                : targetNode.AddDynamicInput(propData.ConnectorType, ConnectionType.Override, fieldName: fieldName);

            return dynamicPort;
        }
        #endregion

        #region Node Connections to Data
        void OnNodeUpdated(INode n)
        {
            //Debug.Log($"Node updated {n.Name}");
            if (!m_nodes.TryGetValue(n, out var mem)) 
                return;
            
            NodeUpdated_ApplyReferencesFromConnections(mem, n);
        }

        void NodeUpdated_ApplyReferencesFromConnections(NodeConnectorMemory mem, INode node)
        {
            var nodeEditor = mem.Editor;
            //Debug.Log($"{nameof(OnNodeUpdated)} [{node.name}]");
            nodeEditor.SerializedObjectData.Update();

            if (ApplyReferencesFromConnections(mem, node) == ChangeCheck.NotChanged)
                return;

            nodeEditor.SerializedObjectData.ApplyModifiedProperties();

            var targetObj = node.NodeData as ScriptableObject;
            EventMessenger.TriggerEvent(new ObjectChanged() { Changed = targetObj, SourceEditor = this });
        }

        static ChangeCheck ApplyReferencesFromConnections(NodeConnectorMemory mem, INode node)
        {
            var nodeEditor = mem.Editor;
            var so = nodeEditor.SerializedObjectData;

            var changes = false;
            var i = 0;

            for (; i < mem.ListMemory.Count; i++)
            {
                var memory = mem.ListMemory[i];
                if (!memory.PropData.SyncData)
                    continue;

                var prop = so.FindProperty(memory.PropData.PropertyPath);
                var size = prop.arraySize;

                for (var j = 0; j < size; j++)
                {
                    var elementProp = prop.GetArrayElementAtIndex(j);

                    memory.PropData.GetNameAndObjProp(elementProp, j, 
                        out var outputName, out var objProp);
                    //Debug.Log($"{node.Name} {relPathObj} {objProp}");

                    if (ApplyReferencesFromConnections(node, objProp, memory.PropData, outputName, out var connectedToData) != ChangeCheck.Changed)
                        continue;

                    changes = true;
                    memory.Ports[j].Data = connectedToData;
                    mem.ListMemory[i] = memory;
                }
            }

            for (i = 0; i < mem.Memory.Count; i++)
            {
                var memory = mem.Memory[i];
                if (!memory.PropData.SyncData)
                    continue;

                var prop = so.FindProperty(memory.PropData.PropertyPath);

                memory.PropData.GetNameAndObjProp(prop, out var outputName, out var objProp);

                if (ApplyReferencesFromConnections(node, objProp, memory.PropData, outputName,
                        out var connectedToData) != ChangeCheck.Changed)
                    continue;

                changes = true;

                memory.Port.Data = connectedToData;
                mem.Memory[i] = memory;
            }

            return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }

        static ChangeCheck ApplyReferencesFromConnections(INode node, SerializedProperty prop, ConnectorPropData propData, 
            string outputName, out INodeData connectedToData)
        {
            connectedToData = null;
            if (prop.propertyType != SerializedPropertyType.ObjectReference || !propData.SyncData)
                return ChangeCheck.NotChanged;

            var dynamicPorts = propData.IO == IO.Output ? node.DynamicOutputs() : node.DynamicInputs();
            var dynamicPort = dynamicPorts.First(port => port.ValueType == propData.ConnectorType
                                                        && port.FieldName.Equals(outputName));
            if (dynamicPort == null)
            {
                Debug.LogWarning($"Port missing {outputName}");
                return ChangeCheck.NotChanged;
            }

            var connectedToNode = (INode)dynamicPort.ConnectedToPortID.Node;
            connectedToData = connectedToNode?.NodeData;
            var connectedTo = (Object)connectedToData;

            if (prop.objectReferenceValue == connectedTo)
                return ChangeCheck.NotChanged;

            //Debug.Log($"Port changed {prop.objectReferenceValue} to {connectedTo}");
            prop.objectReferenceValue = connectedTo;

            return ChangeCheck.Changed;
        }
        #endregion
        
        /// <summary>
        /// When target object has changes we rebuild connections and memory from data
        /// </summary>
        protected void OnTargetChanged_UpdateConnectors(NodeConnectorMemory memory, INode node)
        {
            var so = memory.Editor.SerializedObjectData;
            using (var changedProps = SimplePool<List<ConnectorPropData>>.I.GetScoped())
            {
                foreach (var mem in memory.ListMemory)
                {
                    var prop = so.FindProperty(mem.PropData.PropertyPath);
                    //Debug.Log($"checking prop {mem.PropData.PropertyPath} {prop.arraySize} {mem.Ports.Length}");
                    var arraySizeChanged = prop.arraySize != mem.Ports.Length;
                    if (arraySizeChanged)
                    {
                        changedProps.Obj.Add(new ConnectorPropData(mem.PropData));
                        continue;
                    }

                    if (!mem.PropData.SyncData)
                        continue;

                    for (var i = 0; i < prop.arraySize; i++)
                    {
                        var elProp = prop.GetArrayElementAtIndex(i);
                        var objProp = mem.PropData.GetObjectProp(elProp);
                        if (objProp == null || objProp.propertyType != SerializedPropertyType.ObjectReference)
                            continue;

                        var currentData = (INodeData) objProp.objectReferenceValue;
                        //Debug.Log($"comparing obj {mem.PropData.PropertyPath} {currentData?.name} {mem.Ports[i].Data?.name}");
                        var dataChanged = currentData != mem.Ports[i].Data;
                        if (!dataChanged)
                            continue;

                        changedProps.Obj.Add(new ConnectorPropData(mem.PropData));
                        break;
                    }
                }

                foreach (var mem in memory.Memory)
                {
                    if (!mem.PropData.SyncData)
                        continue;

                    var prop = so.FindProperty(mem.PropData.PropertyPath);
                    var objProp = mem.PropData.GetObjectProp(prop);

                    var current = (INodeData) objProp.objectReferenceValue;
                    //Debug.Log($"comparing obj {mem.PropData.PropertyPath} {current?.name} {mem.Port.Data?.name}");
                    var dataChanged = current != mem.Port.Data;
                    if (!dataChanged)
                        continue;

                    changedProps.Obj.Add(new ConnectorPropData(mem.PropData));
                }

                foreach (var changedProp in changedProps.Obj)
                {
                    var prop = so.FindProperty(changedProp.PropertyPath);
                    if (prop.isArray)
                    {
                        var idx = memory.ListMemory.FindIndex(al => string.Equals(al.PropData.FieldNameFormat,
                            changedProp.FieldNameFormat, StringComparison.Ordinal));
                        var previousPorts = memory.ListMemory[idx].Ports;
                        memory.ListMemory.RemoveAt(idx);

                        UpdateDynamicConnectorLists(memory.Editor, node, prop, changedProp, previousPorts);
                    }
                    else
                    {
                        var idx = memory.Memory.FindIndex(al => string.Equals(al.PropData.FieldNameFormat,
                            changedProp.FieldNameFormat, StringComparison.Ordinal));
                        var previousPort = memory.Memory[idx].Port;
                        memory.Memory.RemoveAt(idx);
                        memory.Memory.RemoveAll(al => string.Equals(al.PropData.FieldNameFormat, changedProp.FieldNameFormat, StringComparison.Ordinal));
                        UpdateDynamicConnector(memory.Editor, node, prop, changedProp, previousPort);
                    }
                }
            }
        }

        #region Editor Init & Destroy
        protected override void Init()
        {
            base.Init();
            AddListener();
        }

        public override void DestroyData()
        {
            RemoveListener();
            base.DestroyData();
        }
        #endregion
        #region Events
        void AddListener()
        {
            EventMessenger.AddListener<ObjectChanged>(this);
            EventMessenger.AddListener<NodeUpdated>(this);
        }
        void RemoveListener()
        {
            EventMessenger.RemoveListener<NodeUpdated>(this);
            EventMessenger.RemoveListener<ObjectChanged>(this);
        }
        public void OnEvent(ObjectChanged eventType)
        {
            if (eventType.SourceEditor == this)
                return;

            //Debug.Log($"{nameof(OnEvent)} {eventType.Changed}");
            if (!(eventType.Changed is INodeData nodeData))
                return;
            
            var node = nodeData.Node;
            //Debug.Log($"Node changed {node.Name}");
            if (node == null)
            {
                //Debug.LogError("Node is null");
                return;
            }
            if (!m_nodes.TryGetValue(node, out var mem)) 
                return;
            mem.Editor.SerializedObjectData.Update();
            OnTargetChanged_UpdateConnectors(mem, node);
        }

        public void OnEvent(NodeUpdated nodeUpdated) => OnNodeUpdated(nodeUpdated.UpdatedNode);
        #endregion
    }
}