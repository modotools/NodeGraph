﻿using System;
using UnityEditor;
using UnityEngine;

namespace nodegraph.Editor.Graph
{
    [CustomEditor(typeof(SceneGraph), true)]
    public class SceneGraphEditor : UnityEditor.Editor
    {
        bool m_removeSafely;
        Type m_graphType;
        // ReSharper disable once InconsistentNaming
        new SceneGraph target => (SceneGraph) base.target;

        public override void OnInspectorGUI()
        {
            if (target.Graph == null)
            {
                if (!GUILayout.Button("New graph", GUILayout.Height(40))) 
                    return;
                if (m_graphType != null)
                    CreateGraph(m_graphType);
                else
                {
                    var graphTypes = typeof(NodeGraph).GetDerivedTypes();
                    var menu = new GenericMenu();
                    foreach (var graphType in graphTypes)
                    {
                        var type = graphType;
                        menu.AddItem(new GUIContent(graphType.Name), false, () => CreateGraph(type));
                    }

                    menu.ShowAsContext();
                }
            }
            else
            {
                if (GUILayout.Button("Open graph", GUILayout.Height(40))) 
                    NodeEditorWindow.Open(target.Graph);

                if (m_removeSafely)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Really remove graph?");
                    GUI.color = new Color(1, 0.8f, 0.8f);

                    if (GUILayout.Button("Remove"))
                    {
                        m_removeSafely = false;
                        Undo.RecordObject(target, "Removed graph");
                        target.Graph = null;
                    }

                    GUI.color = Color.white;
                    if (GUILayout.Button("Cancel")) 
                        m_removeSafely = false;
                    GUILayout.EndHorizontal();
                }
                else
                {
                    GUI.color = new Color(1, 0.8f, 0.8f);
                    if (GUILayout.Button("Remove graph")) 
                        m_removeSafely = true;
                    GUI.color = Color.white;
                }
            }
        }

        void OnEnable()
        {
            var sceneGraphType = target.GetType();
            if (sceneGraphType == typeof(SceneGraph))
            {
                m_graphType = null;
                return;
            }

            var baseType = sceneGraphType.BaseType;
            Debug.Assert(baseType != null, nameof(baseType) + " != null");
            if (baseType.IsGenericType) 
                m_graphType = baseType.GetGenericArguments()[0];
        }

        void CreateGraph(Type type)
        {
            Undo.RecordObject(target, "Create graph");
            target.Graph = (INodeGraph) CreateInstance(type);
            target.Graph.name = target.name + "-graph";
        }
    }
}