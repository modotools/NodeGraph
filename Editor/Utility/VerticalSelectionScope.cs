﻿using System;
using UnityEngine;
using nodegraph.Editor.Data;

namespace nodegraph.Editor.Utility
{
    public readonly struct VerticalScopeWOptionalHighlight : IDisposable
    {
        readonly bool m_isNestedVertical;

        public VerticalScopeWOptionalHighlight(NodeStyles styles,
            Color defaultColor, Color highlightColor, Color activeColor, 
            bool isSelected, bool isActive)
        {
            m_isNestedVertical = isSelected || isActive;

            var prevColor = GUI.color;
            GUI.color = defaultColor;

            var node = m_isNestedVertical ? styles.BGSelected : styles.BGDefault;
            GUILayout.BeginVertical(node);
            if (m_isNestedVertical)
            {
                GUI.color = isActive ? activeColor : highlightColor;
                GUILayout.BeginVertical(styles.SelectedGlow);
            }

            GUI.color = prevColor;
        }

        public void Dispose()
        {
            GUILayout.EndVertical();

            if (m_isNestedVertical)
                GUILayout.EndVertical();
        }
    }
}