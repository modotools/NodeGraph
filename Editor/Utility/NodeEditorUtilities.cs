﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

using TypeToAttribute = System.Collections.Generic.Dictionary<System.Type, System.Attribute>;
using StringToTypeToAttribute = System.Collections.Generic.Dictionary<string, 
        System.Collections.Generic.Dictionary<System.Type, System.Attribute>>;
using TypeToStringToTypeToAttribute = System.Collections.Generic.Dictionary<System.Type, 
    System.Collections.Generic.Dictionary<string, 
        System.Collections.Generic.Dictionary<System.Type, System.Attribute>>>;

using StringToPropAtList = System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<UnityEngine.PropertyAttribute>>;
using TypeToStringToPropAtList = System.Collections.Generic.Dictionary<System.Type, 
    System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<UnityEngine.PropertyAttribute>>>;

namespace nodegraph.Editor.Utility
{
    /// <summary> A set of editor-only utilities and extensions for xNode </summary>
    public static class NodeEditorUtilities
    {
        const string k_scriptIconName = "cs Script Icon";
        /// <summary>C#'s Script Icon [The one MonoBehaviour Scripts have].</summary>
        static readonly Texture2D k_scriptIcon = (EditorGUIUtility.IconContent(k_scriptIconName).image as Texture2D);

        /// Saves Attribute from Type+Field for faster lookup. Resets on recompile.
        static readonly TypeToStringToTypeToAttribute k_typeAttributes = new TypeToStringToTypeToAttribute();
        /// Saves ordered PropertyAttribute from Type+Field for faster lookup. Resets on recompile.
        static readonly TypeToStringToPropAtList k_typeOrderedPropertyAttributes = new TypeToStringToPropAtList();

        public static bool GetAttribute<T>(Type classType, out T attributeOut) where T : Attribute
        {
            var attributes = classType.GetCustomAttributes(typeof(T), false);
            return GetAttribute(attributes, out attributeOut);
        }

        static bool GetAttribute<T>(IReadOnlyList<object> attributes, out T attributeOut) where T : Attribute
        {
            for (var i = 0; i < attributes.Count; i++)
            {
                if (!(attributes[i] is T)) 
                    continue;
                attributeOut = attributes[i] as T;
                return true;
            }
            attributeOut = null;
            return false;
        }

        static bool GetAttribute<T>(Type classType, string fieldName, out T attributeOut) where T : Attribute
        {
            // If we can't find field in the first run, it's probably a private field in a base class.
            var field = classType.GetFieldInfo(fieldName);
            // This shouldn't happen. Ever.
            if (field == null)
            {
                Debug.LogWarning($"Field {fieldName} couldn't be found");
                attributeOut = null;
                return false;
            }
            var attributes = field.GetCustomAttributes(typeof(T), true);
            return GetAttribute(attributes, out attributeOut);
        }

        public static bool HasAttributes<T>(IEnumerable<object> attributes) where T : Attribute
            => attributes.Any(a => a.GetType() == typeof(T));

        public static bool GetCachedAttribute<T>(Type classType, string fieldName, out T attributeOut) where T : Attribute
        {
            if (!k_typeAttributes.TryGetValue(classType, out var typeFields))
            {
                typeFields = new StringToTypeToAttribute();
                k_typeAttributes.Add(classType, typeFields);
            }

            if (!typeFields.TryGetValue(fieldName, out var typeTypes))
            {
                typeTypes = new TypeToAttribute();
                typeFields.Add(fieldName, typeTypes);
            }

            if (!typeTypes.TryGetValue(typeof(T), out var attr))
            {
                if (GetAttribute(classType, fieldName, out attributeOut))
                {
                    typeTypes.Add(typeof(T), attributeOut);
                    return true;
                }

                typeTypes.Add(typeof(T), null);
            }

            if (attr == null)
            {
                attributeOut = null;
                return false;
            }

            attributeOut = attr as T;
            return true;
        }

        public static IEnumerable<PropertyAttribute> GetCachedPropertyAttributes(Type classType, string fieldName)
        {
            if (!k_typeOrderedPropertyAttributes.TryGetValue(classType, out var typeFields))
            {
                typeFields = new StringToPropAtList();
                k_typeOrderedPropertyAttributes.Add(classType, typeFields);
            }

            if (typeFields.TryGetValue(fieldName, out var typeAttributes)) 
                return typeAttributes;

            var field = classType.GetFieldInfo(fieldName);
            var attributes = field.GetCustomAttributes(typeof(PropertyAttribute), true);
            typeAttributes = attributes.Cast<PropertyAttribute>().Reverse().ToList(); //Unity draws them in reverse
            typeFields.Add(fieldName, typeAttributes);

            return typeAttributes;
        }

        public static bool IsMac() => SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX;

        /// <summary> Returns the default name for the node type. </summary>
        public static string NodeDefaultName(Type type)
        {
            var typeName = type.Name;
            // Automatically remove redundant 'Node' postfix
            if (typeName.EndsWith("Node")) 
                typeName = typeName.Substring(0, 
                    typeName.LastIndexOf("Node", StringComparison.Ordinal));

            typeName = ObjectNames.NicifyVariableName(typeName);
            return typeName;
        }

        /// <summary>Creates a new C# Class.</summary>
        [MenuItem("Assets/Create/xNode/Node C# Script", false, 89)]
        [UsedImplicitly]
        public static void CreateNode()
        {
            var guids = AssetDatabase.FindAssets("xNode_NodeTemplate.cs");
            if (guids.Length == 0)
            {
                Debug.LogWarning("xNode_NodeTemplate.cs.txt not found in asset database");
                return;
            }
            var path = AssetDatabase.GUIDToAssetPath(guids[0]);
            CreateFromTemplate(
                "NewNode.cs",
                path
            );
        }

        /// <summary>Creates a new C# Class.</summary>
        [MenuItem("Assets/Create/xNode/NodeGraph C# Script", false, 89)]
        [UsedImplicitly]
        public static void CreateGraph()
        {
            var guids = AssetDatabase.FindAssets("xNode_NodeGraphTemplate.cs");
            if (guids.Length == 0)
            {
                Debug.LogWarning("xNode_NodeGraphTemplate.cs.txt not found in asset database");
                return;
            }
            var path = AssetDatabase.GUIDToAssetPath(guids[0]);
            CreateFromTemplate(
                "NewNodeGraph.cs",
                path
            );
        }

        static void CreateFromTemplate(string initialName, string templatePath)
        {
            ProjectWindowUtil.StartNameEditingIfProjectWindowExists(
                0,
                ScriptableObject.CreateInstance<DoCreateCodeFile>(),
                initialName,
                k_scriptIcon,
                templatePath
            );
        }

        /// Inherits from EndNameAction, must override EndNameAction.Action
        public class DoCreateCodeFile : UnityEditor.ProjectWindowCallback.EndNameEditAction
        {
            public override void Action(int instanceId, string pathName, string resourceFile)
            {
                var o = CreateScript(pathName, resourceFile);
                ProjectWindowUtil.ShowCreatedAsset(o);
            }
        }

        /// <summary>Creates Script from Template's path.</summary>
        static Object CreateScript(string pathName, string templatePath)
        {
            var className = Path.GetFileNameWithoutExtension(pathName)?.Replace(" ", string.Empty);

            var encoding = new UTF8Encoding(true, false);

            if (!File.Exists(templatePath))
            {
                Debug.LogError($"The template file was not found: {templatePath}");
                return null;
            }

            // Read procedures.
            var reader = new StreamReader(templatePath);
            var templateText = reader.ReadToEnd();
            reader.Close();

            // ReSharper disable StringLiteralTypo, CommentTypo
            templateText = templateText.Replace("#SCRIPTNAME#", className);
            templateText = templateText.Replace("#NOTRIM#", string.Empty);
            // You can replace as many tags you make on your templates, just repeat Replace function
            // e.g.:
            // templateText = templateText.Replace("#NEWTAG#", "MyText");
            // Write procedures.
            // ReSharper restore CommentTypo, StringLiteralTypo

            var writer = new StreamWriter(Path.GetFullPath(pathName ?? throw new ArgumentNullException(nameof(pathName))), false, encoding);
            writer.Write(templateText);
            writer.Close();

            AssetDatabase.ImportAsset(pathName);
            return AssetDatabase.LoadAssetAtPath(pathName, typeof(Object));
        }

        /// <summary> Return a prettified type name. </summary>
        public static string PrettyName(this Type type)
        {
            if (type == null) return "null";
            if (type == typeof(object)) return "object";
            if (type == typeof(float)) return "float";
            if (type == typeof(int)) return "int";
            if (type == typeof(long)) return "long";
            if (type == typeof(double)) return "double";
            if (type == typeof(string)) return "string";
            if (type == typeof(bool)) return "bool";
            if (type.IsGenericType)
            {
                var genericType = type.GetGenericTypeDefinition();
                var s = genericType == typeof(List<>) 
                    ? "List" 
                    : type.GetGenericTypeDefinition().ToString();

                var types = type.GetGenericArguments();
                var typeStr = new string[types.Length];
                for (var i = 0; i < types.Length; i++) 
                    typeStr[i] = types[i].PrettyName();
                return s + "<" + string.Join(", ", typeStr) + ">";
            }
            // ReSharper disable once InvertIf
            if (type.IsArray)
            {
                var rank = "";
                for (var i = 1; i < type.GetArrayRank(); i++) 
                    rank += ",";
                var elementType = type.GetElementType();
                Debug.Assert(elementType != null, nameof(elementType) + " != null");

                if (!elementType.IsArray) 
                    return elementType.PrettyName() + "[" + rank + "]";
                
                var s = elementType.PrettyName();
                var stringIdx = s.IndexOf('[');
                return s.Substring(0, stringIdx) + "[" + rank + "]" + s.Substring(stringIdx);
            }

            return type.ToString();
        }

        
        /// <summary> Returns the default creation path for the node type. </summary>
        public static string NodeDefaultPath(Type type)
        {
            var typePath = type.ToString().Replace('.', '/');
            // Automatically remove redundant 'Node' postfix
            if (typePath.EndsWith("Node")) 
                typePath = typePath.Substring(0, 
                    typePath.LastIndexOf("Node", StringComparison.Ordinal));

            typePath = ObjectNames.NicifyVariableName(typePath);
            return typePath;
        }

        #region Currently Unused
        /// <summary> Returns true if this can be cast to <see cref="Type"/></summary>
        public static bool IsCastableTo(this Type from, Type to)
        {
            if (to.IsAssignableFrom(from)) 
                return true;
            var methods = from.GetMethods(BindingFlags.Public | BindingFlags.Static)
                .Where(m => m.ReturnType == to &&
                            (m.Name == "op_Implicit" || m.Name == "op_Explicit")
                );
            return methods.Any();
        }
        #endregion
    }
}
