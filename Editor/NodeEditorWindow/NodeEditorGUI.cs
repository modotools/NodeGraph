﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Core.Types;
using Core.Unity.Interface;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;
using nodegraph.Data;
using nodegraph.Editor.Data;
using nodegraph.Editor.Enums;
using nodegraph.Editor.Interface;
using nodegraph.Editor.Operations;
using nodegraph.Editor.Resources;
using nodegraph.Editor.Utility;
using nodegraph.Operations;
using Object = UnityEngine.Object;

namespace nodegraph.Editor
{
    /// <summary> Contains GUI methods </summary>
    public partial class NodeEditorWindow
    {
        #region constants
        const int k_defaultLabelWidth = 84;
        const int k_toolTipSpacing = 8;

        const string k_remove = "Remove";
        const string k_disconnect = "Disconnect";
        const string k_clearConnections = "Clear Connections";
        #endregion

        #region public fields
        public INodeGraphEditor GraphEditor;
        /// <summary> Executed after all other window GUI. Useful if Zoom is ruining your day. Automatically resets after being run.</summary>
        public event Action OnLateGUI;

        // todo: maybe to special for fsm stuff here:
        // move more window code to graph code/ static operation stuff
        public Object ActiveNode;
        #endregion

        #region Properties
        /// <summary> 19 if docked, 22 if not </summary>
        int TopPadding => IsDocked() ? 19 : 22;
        #endregion


        #region private fields
        readonly List<Object> m_selectionCache = new List<Object>();
        readonly List<INode> m_culledNodes = new List<INode>();

        #endregion

        #region Unity Hooks
        protected virtual void OnGUI()
        {
            var m = GUI.matrix;
            if (Graph == null)
                return;

            ValidateGraphEditor();
            Controls();

            DrawGrid(position, Zoom, PanOffset);
            DrawConnections();
            DrawDraggedConnection();
            DrawNodes();
            DrawSelectionBox();
            DrawTooltip();
            GraphEditor.OnGUI();

            // Run and reset onLateGUI
            if (OnLateGUI != null)
            {
                OnLateGUI();
                OnLateGUI = null;
            }

            GUI.matrix = m;
        }
        #endregion

        #region GUI
        void DrawGrid(Rect rect, float zoom, Vector2 panOffset)
        {
            rect.position = Vector2.zero;

            var center = rect.size / 2f;
            var gridTextures = GraphEditor.GetGridTextures();

            // Offset from origin in tile units
            var xOffset = -(center.x * zoom + panOffset.x) / gridTextures.Width;
            var yOffset = ((center.y - rect.size.y) * zoom + panOffset.y) / gridTextures.Height;

            var tileOffset = new Vector2(xOffset, yOffset);

            // Amount of tiles
            var tileAmountX = Mathf.Round(rect.size.x * zoom) / gridTextures.Width;
            var tileAmountY = Mathf.Round(rect.size.y * zoom) / gridTextures.Height;

            var tileAmount = new Vector2(tileAmountX, tileAmountY);

            // Draw tiled background
            GUI.DrawTextureWithTexCoords(rect, gridTextures.GridTexture, new Rect(tileOffset, tileAmount));
            GUI.DrawTextureWithTexCoords(rect, gridTextures.CrossTexture, new Rect(tileOffset + new Vector2(0.5f, 0.5f), tileAmount));
        }

        void DrawSelectionBox()
        {
            if (m_currentActivity != NodeActivity.DragGrid)
                return;

            var r = GetSelectionBoxClipped();
            Handles.DrawSolidRectangleWithOutline(r, new Color(0, 0, 0, 0.1f), new Color(1, 1, 1, 0.6f));
        }

        /// <summary> Draw a bezier from output to input in grid coordinates </summary>
        void DrawNoodle(NoodleData data, IList<Vector2> gridPoints)
        {
            // convert grid points to window points
            for (var i = 0; i < gridPoints.Count; ++i)
                gridPoints[i] = GridToWindowPosition(gridPoints[i]);

            NoodleGUI.DrawNoodle(data.Gradient, data.Path, data.Stroke, data.Thickness, gridPoints, Zoom);
        }

        
        /// <summary> Draws all connections </summary>
        void DrawConnections()
        {
            m_hoveredReroute = new RerouteReference();
            var mousePos = Event.current.mousePosition;

            var nodes = Graph.R_INodes();

            using (var selection = SimplePool<List<RerouteReference>>.I.GetScoped())
            using (var gridPoints = SimplePool<List<Vector2>>.I.GetScoped())
            {
                UpdatePreSelectionReroutes(selection.Obj);

                foreach (var node in nodes) 
                    DrawConnectionsForNode(node, gridPoints.Obj, selection.Obj, mousePos);

                UpdateSelectionReroutes(selection.Obj);
            }
        }

        void DrawConnectionsForNode(INode node, List<Vector2> gridPoints, ICollection<RerouteReference> selection, Vector2 mousePos)
        {
            // If a null node is found, return. This can happen if the nodes associated script is deleted.
            // It is currently not possible in Unity to delete a null asset.
            if (node == null)
                return;

            // Draw full connections and output > reroute
            foreach (var output in node.Outputs()) 
                DrawConnectionsForOutput(output, gridPoints, selection, mousePos);
        }

        void DrawConnectionsForOutput(NodePort output, List<Vector2> gridPoints, ICollection<RerouteReference> selection, Vector2 mousePos)
        {
            // Needs cleanup. Null checks are ugly
            if (!PortConnectionPoints.TryGetValue(output.ID, out var fromRect))
                return;

            var portColor = GraphEditor.GetPortColor(output);

            //using (var connectionList = SimplePool<List<Connection>>.I.GetScoped())
            //{
            //    connectionList.Obj.AddRange(output.Connections);
            foreach (var c in output.Connections)
                DrawConnection(c, gridPoints, selection, mousePos, fromRect, portColor);
            //}
        }

        void DrawConnection(Connection c, List<Vector2> gridPoints, ICollection<RerouteReference> selection, 
            Vector2 mousePos, Rect fromRect, Color portColor)
        {
            if (!ConnectionValid(c, out var toRect)) 
                return;

            var reroutePoints = c.Graph.GetReroutePoints(c);

            GetGridPoints(gridPoints, fromRect, reroutePoints, toRect);
            DrawNoodle(GraphEditor.GetNoodleData(c), gridPoints);

            // Loop through reroute points again and draw the points
            for (var i = 0; i < reroutePoints.Count; i++)
                DrawReroutePoints(c, selection, mousePos, portColor, reroutePoints, i);
        }
        
        void DrawReroutePoints(Connection c, ICollection<RerouteReference> selection, Vector2 mousePos, Color portColor,
            IReadOnlyList<Vector2> reroutePoints, int rerouteIdx)
        {
            var rerouteRef = new RerouteReference(c, rerouteIdx);
            // Draw reroute point at position
            var rerouteRect = GetRerouteRect(reroutePoints, rerouteIdx);

            // Draw selected reroute points with an outline
            if (SelectedReroutes.Contains(rerouteRef))
            {
                using (new ColorScope(Settings.HighlightColor))
                    GUI.DrawTexture(rerouteRect, NodeEditorResources.DotOuter);
            }

            using (new ColorScope(portColor))
                GUI.DrawTexture(rerouteRect, NodeEditorResources.Dot);

            if (rerouteRect.Overlaps(m_selectionBox))
                selection.Add(rerouteRef);
            if (rerouteRect.Contains(mousePos))
                m_hoveredReroute = rerouteRef;
        }

        void DrawNodes()
        {
            var e = Event.current;

            if (e.type == EventType.Layout)
            {
                CacheSelection();
                m_culledNodes.Clear();
            }            
            else
                ResetHovered();

            var mousePos = e.mousePosition;
            var selectionBox = GetSelectionBox();

            using (var preSelection = SimplePool<List<Object>>.I.GetScoped())
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new ZoomScope(position, Zoom, TopPadding))
            {
                UpdatePreSelection(preSelection.Obj);
                
                foreach (var nodeObj in Graph.NodeObjs)
                    DrawNode(e, nodeObj, mousePos * Zoom, selectionBox, preSelection.Obj);

                UpdateSelection(e, preSelection.Obj);
                Verify(check.changed);
                // todo: check if dispose of zoom is enough or maybe values changed, then we need to sync somehow
            }
        }

        void DrawTooltip()
        {
            if (!Settings.PortTooltips || GraphEditor is null)
                return;

            string tooltip = null;

            if (HoveredPort != null)
                tooltip = GraphEditor.GetPortTooltip(HoveredPort);
            else if (m_hoveredNode != null && IsHoveringNode && IsHoveringTitle(m_hoveredNode))
            {
                var editor = NodeEditorCache.GetEditor(m_hoveredNode, this);
                tooltip = editor.GetHeaderTooltip();
            }

            if (string.IsNullOrEmpty(tooltip))
                return;

            var content = new GUIContent(tooltip);
            var size = NodeEditorResources.Styles.Tooltip.CalcSize(content);
            size.x += k_toolTipSpacing;

            var rect = new Rect(Event.current.mousePosition - size, size);
            EditorGUI.LabelField(rect, content, NodeEditorResources.Styles.Tooltip);

            Repaint();
        }

        void DrawNode(Event e, ScriptableObject nodeObj, Vector2 mousePos, Rect selectionBox,
            ICollection<Object> preSelection)
        {
            var node = (INode) nodeObj;
            // Skip null nodes. The user could be in the process of renaming scripts, so removing them at this point is not advisable.
            if (node == null)
                return;
            if (IsCulled(e, nodeObj, node)) 
                return;

            if (e.type == EventType.Repaint) 
                RemoveCachedPortConnectionPoints(node);

            var nodeEditor = NodeEditorCache.GetEditor(node, this);

            NodeEditorCache.PortPositions.Clear();

            // Set default label width. This is potentially overridden in OnBodyGUI
            EditorGUIUtility.labelWidth = k_defaultLabelWidth;

            // Get node position
            var nodePos = GridToWindowPositionNoClipped(node.Position);

            var areaRect = new Rect(nodePos, new Vector2(nodeEditor.GetWidth(), 4000));
            using (new GUILayout.AreaScope(areaRect))
            {
                var isSelected = m_selectionCache.Contains(nodeObj);
                var isActive = ActiveNode == nodeObj;

                using (new VerticalScopeWOptionalHighlight(nodeEditor.GetStyles(),
                    nodeEditor.GetTint(),
                    Settings.HighlightColor, 
                    Settings.ActiveColor,
                    isSelected, isActive))
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    DrawNode(nodeEditor);

                    // If user changed a value, notify other scripts through onUpdateNode
                    if (check.changed) 
                        OnNodeGUIChanged(nodeObj, node, nodeEditor);
                }

                // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
                switch (e.type)
                {
                    case EventType.Repaint: // Cache data about the node for next frame
                    {
                        CacheNodeGUIData(node);
                        break;
                    }
                    case EventType.Layout: return;
                }

                UpdateHoveringAndDragSelection(mousePos, selectionBox, preSelection, nodePos, node);
                UpdateHoveringPorts(mousePos, node);
            }
        }

        static void DrawNode(INodeEditor nodeEditor)
        {
            // Draw node contents
            nodeEditor.OnHeaderGUI();
            nodeEditor.OnBodyGUI();
        }

        #region Connection GUI methods
        void UpdateSelectionReroutes(List<RerouteReference> selection)
        {
            if (Event.current.type != EventType.Layout && m_currentActivity == NodeActivity.DragGrid)
                SelectedReroutes = selection;
        }

        void UpdatePreSelectionReroutes(List<RerouteReference> selection)
        {
            if (m_preBoxSelectionReroute != null)
                selection.AddRange(m_preBoxSelectionReroute);
        }

        static void GetGridPoints(List<Vector2> gridPoints, Rect fromRect, IEnumerable<Vector2> reroutePoints, Rect toRect)
        {
            gridPoints.Clear();
            gridPoints.Add(fromRect.center);
            gridPoints.AddRange(reroutePoints);
            gridPoints.Add(toRect.center);
        }

        bool ConnectionValid(Connection c, out Rect toRect)
        {
            toRect = default;
            if (c.From.Node == null || c.To.Node == null)
                return false;
            var valid = PortConnectionPoints.TryGetValue(c.To, out toRect);
            return valid;
        }

        Rect GetRerouteRect(IReadOnlyList<Vector2> reroutePoints, int rerouteIdx)
        {
            var rect = new Rect(reroutePoints[rerouteIdx], new Vector2(12, 12));
            rect.position = new Vector2(rect.position.x - 6, rect.position.y - 6);
            rect = GridToWindowRect(rect);
            return rect;
        }
        #endregion

        #region Node GUI methods
        static void Verify(bool changed)
        {
            if (!changed)
                return;
            if (!(Selection.activeObject is IVerify verify))
                return;

            VerificationResult result = default;
            verify.Verify(ref result);

            // todo: show results to user
        }

        void UpdatePreSelection(List<Object> preSelection)
        {
            if (m_preBoxSelection != null)
                preSelection.AddRange(m_preBoxSelection);
        }

        static void UpdateSelection(Event e, List<Object> preSelection)
        {
            if (e.type != EventType.Layout
                && m_currentActivity == NodeActivity.DragGrid) 
                Selection.objects = preSelection.ToArray();

        }

        void CacheSelection()
        {
            m_selectionCache.Clear();
            m_selectionCache.AddRange(Selection.objects);
        }

        Rect GetSelectionBox()
        {
            var curPos = WindowToGridPosition(Event.current.mousePosition);
            var size = curPos - m_dragBoxStart;
            var boxPos = GridToWindowPositionNoClipped(m_dragBoxStart);
            GetPositiveDragBox(ref size, ref boxPos);

            return new Rect(boxPos, size);
        }

        Rect GetSelectionBoxClipped()
        {
            var curPos = WindowToGridPosition(Event.current.mousePosition);
            var size = (curPos - m_dragBoxStart)/Zoom;
            var boxPos = GridToWindowPosition(m_dragBoxStart);
            GetPositiveDragBox(ref size, ref boxPos);

            return new Rect(boxPos, size);
        }

        void UpdateHoveringPorts(Vector2 mousePos, INode node)
        {
            //Check if we are hovering any of this nodes ports
            //Check input ports
            UpdateHoveringPorts(mousePos, node.Inputs());
            //Check all output ports
            UpdateHoveringPorts(mousePos, node.Outputs());
        }

        void UpdateHoveringPorts(Vector2 mousePos, IEnumerable<NodePort> ports)
        {
            foreach (var port in ports)
            {
                //Check if port rect is available
                if (!PortConnectionPoints.ContainsKey(port.ID))
                    continue;
                var r = GridToWindowRectNoClipped(PortConnectionPoints[port.ID]);
                if (r.Contains(mousePos))
                    HoveredPort = port;
            }
        }

        void UpdateHoveringAndDragSelection(Vector2 mousePos, Rect selectionBox, 
            ICollection<Object> preSelection, Vector2 nodePos,
            INode node)
        {
            //Check if we are hovering this node
            var nodeSize = GUILayoutUtility.GetLastRect().size;
            var windowRect = new Rect(nodePos, nodeSize);

            if (windowRect.Contains(mousePos)) 
                m_hoveredNode = node;

            //If dragging a selection box, add nodes inside to selection
            if (m_currentActivity == NodeActivity.DragGrid
                && windowRect.Overlaps(selectionBox))
                preSelection.Add((Object) node);
        }

        void CacheNodeGUIData(INode node)
        {
            var size = GUILayoutUtility.GetLastRect().size;
            if (NodeSizes.ContainsKey(node))
                NodeSizes[node] = size;
            else
                NodeSizes.Add(node, size);

            foreach (var kvp in NodeEditorCache.PortPositions)
            {
                var portHandlePos = kvp.Value;
                portHandlePos += node.Position;
                var rect = new Rect(portHandlePos.x - 8, portHandlePos.y - 8, 16, 16);
                PortConnectionPoints[kvp.Key.ID] = rect;
            }
        }

        static void OnNodeGUIChanged(Object nodeObj, INode node, INodeEditor nodeEditor)
        {
            EventMessenger.TriggerEvent(new NodeUpdated(){ UpdatedNode = node });
            EditorUtility.SetDirty(nodeObj);
            nodeEditor.SerializedObject.ApplyModifiedProperties();
            nodeEditor.OnNodeUpdated();
        }

        bool IsCulled(Event e, Object nodeObj, INode node)
        {
            // Culling
            if (e.type != EventType.Layout) 
                return m_culledNodes.Contains(node);

            // Cull unselected nodes outside view
            if (Selection.Contains(nodeObj) || !ShouldBeCulled(node)) 
                return false;

            m_culledNodes.Add(node);
            return true;
        }
        bool ShouldBeCulled(INode node)
        {
            var nodePos = GridToWindowPositionNoClipped(node.Position);
            if (nodePos.x / m_zoom > position.width)
                return true; // Right
            if (nodePos.y / m_zoom > position.height)
                return true; // Bottom
            if (!NodeSizes.ContainsKey(node))
                return false;

            var size = NodeSizes[node];
            if (nodePos.x + size.x < 0)
                return true; // Left
            return nodePos.y + size.y < 0; //Top
        }

        void RemoveCachedPortConnectionPoints(INode node)
        {
            var nodeObj = (Object) node;
            using (var removeEntriesScoped = SimplePool<List<PortID>>.I.GetScoped())
            {
                var removeEntries = removeEntriesScoped.Obj;

                //foreach (var kvp in PortConnectionPoints) if (kvp.Key.Node == node) removeEntries.Add(kvp.Key);
                removeEntries.AddRange(from kvp in PortConnectionPoints where kvp.Key.Node == nodeObj select kvp.Key);
                foreach (var k in removeEntries)
                    PortConnectionPoints.Remove(k);
            }
        }

        void ResetHovered()
        {
            m_hoveredNode = null;
            HoveredPort = null;
        }
        #endregion

        //public static bool DropdownButton(string name, float width)
        //    => GUILayout.Button(name, EditorStyles.toolbarDropDown, GUILayout.Width(width));
        #endregion

        #region Show ContextMenu
        /// <summary> Show right-click context menu for hovered reroute </summary>
        static void ShowRerouteContextMenu(RerouteReference reroute)
        {
            var contextMenu = new GenericMenu();
            contextMenu.AddItem(new GUIContent(k_remove), false, reroute.RemovePoint);
            contextMenu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
            if (Settings.AutoSave)
                AssetDatabase.SaveAssets();
        }

        /// <summary> Show right-click context menu for hovered port </summary>
        static void ShowPortContextMenu(NodePort hoveredPort)
        {
            var contextMenu = new GenericMenu();
            foreach (var c in hoveredPort.Connections)
            {
                var portID = hoveredPort.GetConnectedPortID(c);
                var name = portID.Node.name;
                contextMenu.AddItem(new GUIContent($"{k_disconnect}({name})"), false, () => c.Graph.Disconnect(hoveredPort.ID, portID));
            }
            contextMenu.AddItem(new GUIContent(k_clearConnections), false, hoveredPort.ClearConnections);
            contextMenu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
            if (Settings.AutoSave)
                AssetDatabase.SaveAssets();
        }
        #endregion
    }
}
