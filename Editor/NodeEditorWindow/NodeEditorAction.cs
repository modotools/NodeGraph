using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Tools;
using Core.Events;
using UnityEditor;
using UnityEngine;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Editor.Cache;
using nodegraph.Editor.Data;
using nodegraph.Editor.Enums;
using nodegraph.Editor.Operations;
using nodegraph.Editor.Popup;
using nodegraph.Editor.Utility;
using static nodegraph.Operations.NodeOperations;

using Object = UnityEngine.Object;

namespace nodegraph.Editor
{
    public partial class NodeEditorWindow
    {
        #region constants
        // move to core
        enum MouseButton
        {
            Left,
            Right,
            Middle,
            Forward,
            Back
        }

        const string k_softDeleteCmd = "SoftDelete";
        const string k_deleteCmd = "Delete";
        const string k_duplicateCmd = "Duplicate";
        const string k_copyCmd = "Copy";
        const string k_pasteCmd = "Paste";

        const KeyCode k_resetViewKey = KeyCode.F;
        const KeyCode k_renameKey = KeyCode.F2;
        const KeyCode k_renameKeyMac = KeyCode.Return;
        const KeyCode k_selectDeselectAllKey = KeyCode.A;

        const int k_gridA = 8;
        const int k_gridB = 2 * k_gridA;
        #endregion

        #region public fields
        public static INode[] CopyBuffer;

        public NodePort HoveredPort;
        public List<RerouteReference> SelectedReroutes = new List<RerouteReference>();
        #endregion

        #region statics
        static NodeActivity m_currentActivity = NodeActivity.Idle;
        static Vector2[] m_dragOffset;
        static bool m_isPanning;
        #endregion

        #region properties
        bool IsDraggingPort => m_draggedOutput != null;
        bool IsHoveringPort => HoveredPort != null;
        bool IsHoveringNode => m_hoveredNode != null;
        bool IsHoveringReroute => m_hoveredReroute.Connection.Graph != null;
        static KeyCode RenameKey => NodeEditorUtilities.IsMac() ? k_renameKeyMac : k_renameKey;
        #endregion

        #region private fields
        INode m_hoveredNode;
        NodePort m_draggedOutput;
        NodePort m_draggedOutputTarget;
        NodePort m_autoConnectOutput;
        List<Vector2> m_draggedOutputReroutes = new List<Vector2>();

        RerouteReference m_hoveredReroute;
        Vector2 m_dragBoxStart;
        Object[] m_preBoxSelection;
        RerouteReference[] m_preBoxSelectionReroute;
        Rect m_selectionBox;
        bool m_isDoubleClick;
        Vector2 m_lastMousePosition;
        float m_dragThreshold = 1f;
        #endregion

        #region Controls

        bool m_doRepaint;
        //string m_debug;
        void Controls()
        {
            m_doRepaint = false;
            //m_debug = "";

            wantsMouseMove = true;
            var e = Event.current;
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            switch (e.type)
            {
                case EventType.DragUpdated: OnDragUpdate(); break;
                case EventType.DragPerform: OnDragPerform(); break;
                case EventType.MouseMove: OnMouseMove(e); break;
                case EventType.ScrollWheel: OnScrollWheel(e); break;
                case EventType.MouseDrag: OnMouseDrag(e); break;
                case EventType.MouseDown: OnMouseDown(e); break;
                case EventType.MouseUp: OnMouseUp(e); break;
                case EventType.KeyDown: OnKeyDown(e); break;
                case EventType.ValidateCommand: OnValidateCommand(e); break;
                case EventType.ExecuteCommand: OnCommand(e); break;
                case EventType.Ignore: OnIgnoredEvent(e); break;
            }
            //if(!string.IsNullOrEmpty(m_debug))
            //    Debug.Log(m_debug);
            if (m_doRepaint)
                Repaint();
        }

        void OnMouseMove(Event e) => m_lastMousePosition = e.mousePosition;
        void OnScrollWheel(Event e)
        {
            var oldZoom = Zoom;
            var dir = e.delta.y > 0 ? 1 : -1;
            Zoom += dir * (0.1f * Zoom);
            if (Settings.ZoomToMouse)
                PanOffset += (1 - oldZoom / Zoom) * (WindowToGridPosition(e.mousePosition) + PanOffset);
        }

        void OnDragPerform()
        {
            DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
            DragAndDrop.AcceptDrag();
            GraphEditor.OnDropObjects(DragAndDrop.objectReferences);
        }

        static void OnDragUpdate() => DragAndDrop.visualMode = DragAndDropVisualMode.Generic;

        void OnIgnoredEvent(Event e)
        {
            // If release mouse outside window
            if (e.rawType != EventType.MouseUp 
                || m_currentActivity != NodeActivity.DragGrid) 
                return;

            m_doRepaint = true;
            m_currentActivity = NodeActivity.Idle;
        }

        void OnValidateCommand(Event e)
        {
            switch (e.commandName)
            {
                case k_softDeleteCmd:
                case k_deleteCmd when NodeEditorUtilities.IsMac():
                case k_duplicateCmd:
                case k_copyCmd:
                case k_pasteCmd:
                    e.Use();
                    break;
            }
            m_doRepaint = true;
        }

        void OnCommand(Event e)
        {
            switch (e.commandName)
            {
                case k_softDeleteCmd:
                {
                    RemoveSelectedNodes();
                    e.Use();
                    break;
                }
                case k_deleteCmd when NodeEditorUtilities.IsMac():
                {
                    RemoveSelectedNodes();
                    e.Use();
                    break;
                }
                case k_duplicateCmd:
                {
                    DuplicateSelectedNodes();
                    e.Use();
                    break;
                }
                case k_copyCmd:
                {
                    CopySelectedNodes();
                    e.Use();
                    break;
                }
                case k_pasteCmd:
                {
                    PasteNodes(WindowToGridPosition(m_lastMousePosition));
                    e.Use();
                    break;
                }
            }

            m_doRepaint = true;
        }

        void OnKeyDown(Event e)
        {
            if (EditorGUIUtility.editingTextField) 
                return;
            if (e.keyCode == k_resetViewKey) 
                ResetView();

            
            if (e.keyCode == RenameKey)
                RenameSelectedNode();

            if (e.keyCode != k_selectDeselectAllKey) 
                return;

            if (Selection.objects.Any(x => Graph.NodeObjs.Contains(x)))
                DeselectAll();
            else
                SelectAll();

            m_doRepaint = true;
        }

        void OnMouseUp(Event e)
        {
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            switch ((MouseButton) e.button)
            {
                case MouseButton.Left:
                {
                    OnMouseDefaultRelease(e);
                    m_doRepaint = true;
                    m_currentActivity = NodeActivity.Idle;
                    break;
                }
                case MouseButton.Right:
                case MouseButton.Middle:
                {
                    OnMouseAltRelease(e);
                    break;
                }
            }

            // Reset DoubleClick
            m_isDoubleClick = false;
        }

        void OnMouseDown(Event e)
        {
            //m_debug += $"{nameof(OnMouseDown)} {e.button} {e.type}\n";
            m_doRepaint = true;
            if (e.button != (int) MouseButton.Left) 
                return;

            OnMouseDefaultDown(e);
        }
        void OnMouseDrag(Event e)
        {
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            switch ((MouseButton) e.button)
            {
                case MouseButton.Left:
                {
                    OnMouseDragDefault(e); return;
                }
                case MouseButton.Right:
                case MouseButton.Middle:
                {
                    OnMouseDragAlt(e); return;
                }
            }
        }

        #region Mouse Button Specification
        void OnMouseDefaultDown(Event e)
        {
            //m_debug += $"{nameof(OnMouseDefaultDown)}\n";

            m_draggedOutputReroutes.Clear();

            if (IsHoveringPort)
            {
                OnMouseDownOnPort();
                return;
            }

            if (IsHoveringNode && IsHoveringTitle(m_hoveredNode))
            {
                OnMouseDownOnNode(e);
                return;
            }

            if (IsHoveringReroute)
            {
                OnMouseDownOnReroute(e);
                return;
            }

            if (IsHoveringNode) 
                return;

            OnMouseDownGrid(e);
        }

        void OnMouseDragAlt(Event e)
        {
            //check drag threshold for larger screens
            if (e.delta.magnitude <= m_dragThreshold) 
                return;

            PanOffset += e.delta * Zoom;
            m_isPanning = true;
        }

        void OnMouseDragDefault(Event e)
        {
            if (IsDraggingPort)
                OnMouseDragPort();
            else if (m_currentActivity == NodeActivity.HoldNode) 
                OnHoldNodeToDragNode(e);

            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            switch (m_currentActivity)
            {
                case NodeActivity.DragNode:
                {
                    OnMouseDragNode(e);
                    break;
                }
                case NodeActivity.HoldGrid:
                    OnHoldGridToDragGrid(e);
                    break;
                case NodeActivity.DragGrid:
                {
                    OnMouseDragGrid(e);
                    break;
                }
            }
        }

                /// <summary>
        /// Alternative Button (right || middle mouse button released)
        /// </summary>
        /// <param name="e">the event</param>
        void OnMouseAltRelease(Event e)
        {
            if (m_isPanning)
            {
                m_isPanning = false;
                return;
            }

            if (IsDraggingPort)
            {
                m_draggedOutputReroutes.Add(WindowToGridPosition(e.mousePosition));
                return;
            }

            // todo: what is this case? what are selected reroutes
            if (m_currentActivity == NodeActivity.DragNode && Selection.activeObject == null &&
                SelectedReroutes.Count == 1)
            {
                SelectedReroutes[0].InsertPoint(SelectedReroutes[0].GetPoint());
                SelectedReroutes[0] = new RerouteReference(SelectedReroutes[0].Connection, SelectedReroutes[0].PointIndex + 1);
                return;
            }

            if (IsHoveringReroute)
            {
                ShowRerouteContextMenu(m_hoveredReroute);
                return;
            }

            if (IsHoveringPort)
            {
                ShowPortContextMenu(HoveredPort);
                return;
            }

            if (!IsHoveringNode)
            {
                m_autoConnectOutput = null;
                var menu = new GenericMenu();
                GraphEditor.AddContextMenuItems(menu);
                menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                return;
            }

            // IsHoveringNode:
            if (!IsHoveringTitle(m_hoveredNode)) 
                return;
            {
                if (!Selection.Contains((Object) m_hoveredNode))
                    SelectNode((Object) m_hoveredNode, false);
                m_autoConnectOutput = null;
                var menu = new GenericMenu();

                var nodeGraphEditor = NodeEditorCache.GetEditor(Graph, this);
                nodeGraphEditor.AddContextMenuItemsWithNodesSelected(menu);

                var nodeEditor = NodeEditorCache.GetEditor(m_hoveredNode, this);
                nodeEditor.AddContextMenuItems(menu);

                menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                e.Use(); // Fixes copy/paste context menu appearing in Unity 5.6.6f2 - doesn't occur in 2018.3.2f1 Probably needs to be used in other places.
            }
        }

        /// <summary>
        /// Default mouse button (left) release
        /// </summary>
        /// <param name="e">the event</param>
        void OnMouseDefaultRelease(Event e)
        {
            //todo: check if works after refactor, not all actions were disconnected with return/else, changed order

            var ctrShift = e.control || e.shift;
            if (IsDraggingPort)
            {
                OnPortDragRelease();
                return;
            }
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            switch (m_currentActivity)
            {
                case NodeActivity.DragNode: OnDragNodeRelease(); return;
                // If click node header, select it.
                case NodeActivity.HoldNode when !ctrShift: OnClickNodeHeader(); return;
            }
            // If click reroute, select it.
            if (IsHoveringReroute && !ctrShift)
            {
                OnClickedReroute();
                return;
            }
            if (!IsHoveringNode) 
                OnClickedEmpty();
        }
        #endregion

        #endregion
        
        #region Clicked 
        static void OnClickedEmpty()
        {
            // If click outside node, release field focus
            if (!m_isPanning)
            {
                EditorGUI.FocusTextInControl(null);
                EditorGUIUtility.editingTextField = false;
            }

            if (Settings.AutoSave)
                AssetDatabase.SaveAssets();
        }

        void OnClickedReroute()
        {
            SelectedReroutes = new List<RerouteReference>() { m_hoveredReroute };
            Selection.activeObject = null;
        }

        void OnClickNodeHeader()
        {
            if (m_hoveredNode == null)
                return;

            SelectedReroutes.Clear();
            SelectNode((Object) m_hoveredNode, false);

            // Double click to center node
            if (!m_isDoubleClick) 
                return;

            if (m_hoveredNode is INestedGraph nested)
            {
                var w = Open(nested.NestedGraph);
                Selection.activeObject = (Object) nested.NestedGraph;
                w.ResetView(); // Focus selected node
                return;
            }

            var nodeDimension = NodeSizes.ContainsKey(m_hoveredNode)
                ? NodeSizes[m_hoveredNode] / 2
                : Vector2.zero;
            PanOffset = -m_hoveredNode.Position - nodeDimension;
        }
        #endregion

        #region Released
        static void OnDragNodeRelease()
        {
            var nodes = Selection.objects.OfType<INode>();
            foreach (var node in nodes)
                EditorUtility.SetDirty((Object) node);
            if (Settings.AutoSave)
                AssetDatabase.SaveAssets();
        }

        void OnPortDragRelease()
        {
            // If connection is valid, save it
            var connectionValid = m_draggedOutputTarget != null 
                                  && m_draggedOutput.CanConnectTo(m_draggedOutputTarget);

            var autoConnectNoTarget = m_draggedOutputTarget == null 
                                      && Settings.DragToCreate 
                                      && m_autoConnectOutput != null;

            if (connectionValid)
                SaveDraggedConnection();
            // Open context menu for auto-connection if there is no target node
            else if (autoConnectNoTarget)
                AutoConnectNoTargetContextMenu();

            ReleaseDraggedConnection();
        }

        void ReleaseDraggedConnection()
        {
            m_draggedOutput = null;
            m_draggedOutputTarget = null;

            EditorUtility.SetDirty((Object) Graph);

            if (Settings.AutoSave)
                AssetDatabase.SaveAssets();
        }
        #endregion

        #region Mouse Down
        void OnMouseDownGrid(Event e)
        {
            //m_debug += $"{nameof(OnMouseDownGrid)}\n";

            m_currentActivity = NodeActivity.HoldGrid;
            if (e.control || e.shift) 
                return;

            // If mousedown on grid background, deselect all
            SelectedReroutes.Clear();

            Selection.activeObject = null;
            Selection.activeObject = (Object) Graph;
        }

        void OnMouseDownOnReroute(Event e)
        {
            //m_debug += $"{nameof(OnMouseDownOnReroute)}\n";

            var ctrlShift = e.control || e.shift;
            // If reroute isn't selected
            if (!SelectedReroutes.Contains(m_hoveredReroute))
            {
                // Add it
                if (ctrlShift)
                    SelectedReroutes.Add(m_hoveredReroute);
                // Select it
                else
                {
                    SelectedReroutes = new List<RerouteReference>() {m_hoveredReroute};
                    Selection.activeObject = null;
                }
            }
            // Deselect
            else if (ctrlShift)
                SelectedReroutes.Remove(m_hoveredReroute);

            e.Use();
            m_currentActivity = NodeActivity.HoldNode;
        }

        void OnMouseDownOnNode(Event e)
        {
            //m_debug += $"{nameof(OnMouseDownOnNode)}\n";

            // If mousedown on node header, select or deselect
            if (!Selection.Contains((Object) m_hoveredNode))
            {
                SelectNode((Object) m_hoveredNode, e.control || e.shift);
                if (!e.control && !e.shift)
                    SelectedReroutes.Clear();
            }
            else if (e.control || e.shift)
                DeselectNode((Object) m_hoveredNode);

            // Cache double click state, but only act on it in MouseUp - Except ClickCount only works in mouseDown.
            m_isDoubleClick = (e.clickCount == 2);

            e.Use();
            m_currentActivity = NodeActivity.HoldNode;
        }

        void OnMouseDownOnPort()
        {
            //m_debug += $"{nameof(OnMouseDownOnPort)}\n";

            if (HoveredPort.IsOutput)
            {
                m_draggedOutput = HoveredPort;
                m_autoConnectOutput = HoveredPort;
                return;
            }

            //HoveredPort.VerifyConnections();
            m_autoConnectOutput = null;

            if (!HoveredPort.IsConnected)
                return;

            var node = HoveredPort.Node;
            var output = Graph.GetPort(HoveredPort.ConnectedPortID);

            var from = output;
            var to = HoveredPort;
            NodePortOperations.FixDirection(ref from, ref to);

            var points = Graph.GetReroutePoints(new Connection(){ From = from.ID, To = to.ID });
            m_draggedOutputReroutes = points ?? m_draggedOutputReroutes;
            //Debug.Log($"Disconnect HoveredPort!");
            NodePortOperations.Disconnect(HoveredPort, output);

            m_draggedOutput = output;
            m_draggedOutputTarget = HoveredPort;

            EventMessenger.TriggerEvent(new NodeUpdated(){ UpdatedNode = from.Node });
            EventMessenger.TriggerEvent(new NodeUpdated(){ UpdatedNode = to.Node });
        }
        #endregion

        #region Drag
        void OnMouseDragGrid(Event e)
        {
            var boxStartPos = GridToWindowPosition(m_dragBoxStart);
            var boxSize = e.mousePosition - boxStartPos;
            GetPositiveDragBox(ref boxSize, ref boxStartPos);

            m_selectionBox = new Rect(boxStartPos, boxSize);
            m_doRepaint = true;
        }

        void OnHoldGridToDragGrid(Event e)
        {
            m_currentActivity = NodeActivity.DragGrid;
            m_preBoxSelection = Selection.objects;
            m_preBoxSelectionReroute = SelectedReroutes.ToArray();
            m_dragBoxStart = WindowToGridPosition(e.mousePosition);
            m_doRepaint = true;
        }

        void OnMouseDragNode(Event e)
        {
            // Holding ctrl inverts grid snap
            var gridSnap = Settings.GridSnap;
            if (e.control)
                gridSnap = !gridSnap;

            var mousePos = WindowToGridPosition(e.mousePosition);

            // Move selected nodes with offset
            for (var i = 0; i < Selection.objects.Length; i++)
            {
                var nodeObj = Selection.objects[i];
                if (!(nodeObj is INode node))
                    continue;

                Undo.RecordObject(nodeObj, "Moved Node");

                var initial = node.Position;
                var pos = mousePos + m_dragOffset[i];
                if (gridSnap)
                {
                    pos.x = (Mathf.Round((pos.x + k_gridA) / k_gridB) * k_gridB) - k_gridA;
                    pos.y = (Mathf.Round((pos.y + k_gridA) / k_gridB) * k_gridB) - k_gridA;
                }

                node.Position = pos;

                OffsetPortConnections(node, initial);
            }

            MoveSelectedReroutes(mousePos, gridSnap);
            m_doRepaint = true;
        }

        void OnHoldNodeToDragNode(Event e)
        {
            RecalculateDragOffsets(e);
            m_currentActivity = NodeActivity.DragNode;
            m_doRepaint = true;
        }

        void OnMouseDragPort()
        {
            // Set target even if we can't connect, so as to prevent auto-conn menu from opening erroneously
            if (IsHoveringPort && HoveredPort.IsInput &&
                !m_draggedOutput.IsConnectedTo(HoveredPort))
                m_draggedOutputTarget = HoveredPort;
            else
                m_draggedOutputTarget = null;

            m_doRepaint = true;
        }
        #endregion

        #region Actions & Utility
        void SelectAll()
        {
            foreach (var node in Graph.NodeObjs)
                SelectNode(node, true);
        }

        void DeselectAll()
        {
            foreach (var node in Graph.NodeObjs)
                DeselectNode(node);
        }
        void AutoConnectNoTargetContextMenu()
        {
            var menu = new GenericMenu();
            GraphEditor.AddContextMenuItems(menu);
            menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
        }

        void SaveDraggedConnection()
        {
            //Debug.Log($"{nameof(SaveDraggedConnection)}");

            var node = m_draggedOutputTarget.Node;
            if (Graph.NodeObjs.Count() != 0)
                NodePortOperations.Connect(m_draggedOutput, m_draggedOutputTarget);

            var from = m_draggedOutput;
            var to = m_draggedOutputTarget;
            NodePortOperations.FixDirection(ref from, ref to);
            var p = Graph.GetReroutePoints(new Connection {From = from.ID, To = to.ID});
            // ConnectionIndex can be missing if the connection is removed instantly after creation
            if (p == null) 
                return;

            p.AddRange(m_draggedOutputReroutes);
            EventMessenger.TriggerEvent(new NodeUpdated(){ UpdatedNode = from.Node });
            EventMessenger.TriggerEvent(new NodeUpdated(){ UpdatedNode = to.Node });
            EditorUtility.SetDirty((Object) Graph);
        }

        static void GetPositiveDragBox(ref Vector2 boxSize, ref Vector2 boxStartPos)
        {
            if (boxSize.x < 0)
            {
                boxStartPos.x += boxSize.x;
                boxSize.x = Mathf.Abs(boxSize.x);
            }
            // ReSharper disable once InvertIf
            if (boxSize.y < 0)
            {
                boxStartPos.y += boxSize.y;
                boxSize.y = Mathf.Abs(boxSize.y);
            }
        }

        void MoveSelectedReroutes(Vector2 mousePos, bool gridSnap)
        {
            // Move selected reroutes with offset
            for (var i = 0; i < SelectedReroutes.Count; i++)
            {
                var pos = mousePos + m_dragOffset[Selection.objects.Length + i];
                if (gridSnap)
                {
                    pos.x = (Mathf.Round(pos.x / 16) * 16);
                    pos.y = (Mathf.Round(pos.y / 16) * 16);
                }

                SelectedReroutes[i].SetPoint(pos);
            }
        }

        void OffsetPortConnections(INode node, Vector2 initial)
        {
            // Offset portConnectionPoints instantly if a node is dragged so they aren't delayed by a frame.
            var offset = node.Position - initial;
            if (!(offset.sqrMagnitude > 0))
                return;

            foreach (var output in node.Outputs())
            {
                if (!PortConnectionPoints.TryGetValue(output.ID, out var rect))
                    continue;
                rect.position += offset;
                PortConnectionPoints[output.ID] = rect;
            }

            foreach (var input in node.Inputs())
            {
                if (!PortConnectionPoints.TryGetValue(input.ID, out var rect))
                    continue;
                rect.position += offset;
                PortConnectionPoints[input.ID] = rect;
            }
        }

        void RecalculateDragOffsets(Event currentEvent)
        {
            m_dragOffset = new Vector2[Selection.objects.Length + SelectedReroutes.Count];
            // Selected nodes
            for (var i = 0; i < Selection.objects.Length; i++)
            {
                var nodeObj = Selection.objects[i];
                if (!(nodeObj is INode node)) 
                    continue;
                m_dragOffset[i] = node.Position - WindowToGridPosition(currentEvent.mousePosition);
            }

            // Selected reroutes
            for (var i = 0; i < SelectedReroutes.Count; i++)
                m_dragOffset[Selection.objects.Length + i] = SelectedReroutes[i].GetPoint() -
                                                           WindowToGridPosition(currentEvent.mousePosition);
        }

        /// <summary> Puts all selected nodes in focus. If no nodes are present, resets view and zoom to to origin </summary>
        public void ResetView()
        {
            var nodes = Selection.objects.Where(o => o is Node).Cast<Node>().ToList();
            if (nodes.Count > 0)
            {
                var minPos = nodes.Select(x => x.Position).Aggregate((x, y) 
                    => new Vector2(Mathf.Min(x.x, y.x), Mathf.Min(x.y, y.y)));
                var maxPos = nodes.Select(x => x.Position + (NodeSizes.ContainsKey(x) ? NodeSizes[x] : Vector2.zero)).Aggregate((x, y) 
                    => new Vector2(Mathf.Max(x.x, y.x), Mathf.Max(x.y, y.y)));
                PanOffset = -(minPos + (maxPos - minPos) / 2f);
                return;
            }

            Zoom = 2;
            PanOffset = Vector2.zero;
        }

        /// <summary> Remove nodes in the graph in Selection.objects</summary>
        public void RemoveSelectedNodes()
        {
            // We need to delete reroutes starting at the highest point index to avoid shifting indices
            SelectedReroutes = SelectedReroutes.OrderByDescending(x => x.PointIndex).ToList();
            for (var i = 0; i < SelectedReroutes.Count; i++) 
                SelectedReroutes[i].RemovePoint();
            SelectedReroutes.Clear();
            foreach (var item in Selection.objects)
            {
                if (!(item is INode node)) 
                    continue;
                GraphEditor.RemoveNode(node);
            }
        }

        /// <summary> Initiate a rename on the currently selected node </summary>
        public void RenameSelectedNode()
        {
            if (Selection.objects.Length != 1 
                || !(Selection.activeObject is INode node)) 
                return;
            if (NodeSizes.TryGetValue(node, out var size))
                RenamePopup.Show(Selection.activeObject, size.x);
            else
                RenamePopup.Show(Selection.activeObject);
        }

        /// <summary> Draw this node on top of other nodes by placing it last in the graph.nodes list </summary>
        public void MoveNodeToTop(INode node)
        {
            var nodeObj = node as ScriptableObject;
            int index;
            while ((index = Array.IndexOf(Graph.NodeObjs, nodeObj)) != Graph.NodeObjs.Length - 1)
            {
                Graph.NodeObjs[index] = Graph.NodeObjs[index + 1];
                Graph.NodeObjs[index + 1] = nodeObj;
            }
        }

        /// <summary> Duplicate selected nodes and select the duplicates </summary>
        public void DuplicateSelectedNodes()
        {
            // Get selected nodes which are part of this graph
            var selectedNodes = Selection.objects.OfType<INode>().Where(x => x.Graph == Graph).ToArray();
            if (selectedNodes.Length == 0) 
                return;
            // Get top left node position
            var topLeftNode = selectedNodes.Select(x => x.Position).Aggregate((x, y) 
                => new Vector2(Mathf.Min(x.x, y.x), Mathf.Min(x.y, y.y)));
            InsertDuplicateNodes(selectedNodes, topLeftNode + new Vector2(30, 30));
        }

        public void CopySelectedNodes() 
            => CopyBuffer = Selection.objects.OfType<INode>().Where(x => x.Graph == Graph).ToArray();

        public void PasteNodes(Vector2 pos) 
            => InsertDuplicateNodes(CopyBuffer, pos);

        void InsertDuplicateNodes(IReadOnlyList<INode> nodes, Vector2 topLeft)
        {
            //Debug.Log($"{nameof(InsertDuplicateNodes)}");

            if (nodes == null || nodes.Count == 0) 
                return;

            // Get top-left node
            var topLeftNode = nodes.Select(x => x.Position).Aggregate((x, y) 
                => new Vector2(Mathf.Min(x.x, y.x), Mathf.Min(x.y, y.y)));
            var offset = topLeft - topLeftNode;

            var newNodes = new Object[nodes.Count];
            var substitutes = new Dictionary<INode, INode>();
            for (var i = 0; i < nodes.Count; i++)
            {
                var srcNode = nodes[i];
                if (srcNode == null) continue;

                // Check if user is allowed to add more of given node type
                var nodeType = srcNode.GetType();
                if (NodeEditorUtilities.GetAttribute(nodeType, out DisallowMultipleNodesAttribute disallowAttribute))
                {
                    var typeCount = Graph.NodeObjs.Count(x => x.GetType() == nodeType);
                    if (typeCount >= disallowAttribute.Max) 
                        continue;
                }

                var newNode = GraphEditor.CopyNode(srcNode);
                substitutes.Add(srcNode, newNode);
                newNode.Position = srcNode.Position + offset;
                newNodes[i] = (Object) newNode;
            }

            // Walk through the selected nodes again, recreate connections, using the new nodes
            foreach (var srcNode in nodes)
            {
                if (srcNode == null) 
                    continue;
                foreach (var port in srcNode.Ports())
                {
                    foreach (var c in port.Connections)
                    {
                        var inputPort = port.Direction == IO.Input ? port : port.GetConnectedPort(c);
                        var outputPort = port.Direction == IO.Output ? port : port.GetConnectedPort(c);

                        if (substitutes.TryGetValue(inputPort.Node, out var newNodeIn) 
                            && substitutes.TryGetValue(outputPort.Node, out var newNodeOut))
                        {
                            NodeDataCache.I.UpdatePorts(newNodeIn);
                            NodeDataCache.I.UpdatePorts(newNodeOut);

                            inputPort = newNodeIn.GetInputPort(inputPort.FieldName);
                            outputPort = newNodeOut.GetOutputPort(outputPort.FieldName);
                        }
                        if (!inputPort.IsConnectedTo(outputPort)) 
                            NodePortOperations.Connect(outputPort, inputPort);
                    }
                }
            }
            EditorUtility.SetDirty((Object) Graph);
            // Select the new nodes
            //Debug.Log($"Selected {newNodes}");
            Selection.objects = newNodes;
        }

        /// <summary> Draw a connection as we are dragging it </summary>
        void DrawDraggedConnection()
        {
            if (!IsDraggingPort) 
                return;

            if (!PortConnectionPoints.TryGetValue(m_draggedOutput.ID, out var fromRect)) 
                return;

            var noodleData = GraphEditor.GetNoodleData(null, m_draggedOutput);

            var gridPoints = new List<Vector2> {fromRect.center};
            gridPoints.AddRange(m_draggedOutputReroutes);
            gridPoints.Add(m_draggedOutputTarget != null && PortConnectionPoints.ContainsKey(m_draggedOutputTarget.ID)
                ? PortConnectionPoints[m_draggedOutputTarget.ID].center
                : WindowToGridPosition(Event.current.mousePosition));

            DrawNoodle(noodleData, gridPoints);

            var bgCol = Color.black;
            var frCol = noodleData.Gradient.colorKeys[0].color;
            bgCol.a = 0.6f;
            frCol.a = 0.6f;

            // Loop through reroute points again and draw the points
            foreach (var v2 in m_draggedOutputReroutes)
            {
                // Draw reroute point at position
                var rect = new Rect(v2, new Vector2(16, 16));
                rect.position = new Vector2(rect.position.x - 8, rect.position.y - 8);
                rect = GridToWindowRect(rect);

                NodeEditorGUILayout.DrawPortHandle(rect, bgCol, frCol);
            }
        }

        bool IsHoveringTitle(INode node)
        {
            var mousePos = Event.current.mousePosition;
            //Get node position
            var nodePos = GridToWindowPosition(node.Position);
            var width = NodeSizes.TryGetValue(node, out var size) 
                ? size.x 
                : DefaultNodeSize;

            var windowRect = new Rect(nodePos, new Vector2(width / Zoom, 30 / Zoom));
            return windowRect.Contains(mousePos);
        }

        /// <summary> Attempt to connect dragged output to target node </summary>
        public void AutoConnect(INode node)
        {
            //Debug.Log($"{nameof(AutoConnect)}");

            if (m_autoConnectOutput == null) 
                return;

            // Find input port of same type
            var inputPort = node.Ports().FirstOrDefault(x => x.IsInput && x.ValueType == m_autoConnectOutput.ValueType) ??
                            // Fallback to input port
                            node.Ports().FirstOrDefault(x => x.IsInput);
            // Auto-connect if connection is compatible
            if (inputPort != null && inputPort.CanConnectTo(m_autoConnectOutput))
                NodePortOperations.Connect(m_autoConnectOutput, inputPort);

            // Save changes
            EditorUtility.SetDirty((Object) Graph);
            if (Settings.AutoSave) 
                AssetDatabase.SaveAssets();
            m_autoConnectOutput = null;
        }
        #endregion

        public void ShowCreateNestedGraphPopup(Vector2 gridPos) 
            => CreateNestedGraphWindow.Show(Graph, Selection.objects.OfType<INode>());
    }
}
