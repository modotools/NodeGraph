﻿namespace nodegraph.Editor.Data
{
    /// <summary> Fires every whenever a node was modified through the editor </summary>
    public struct NodeUpdated
    {
        public INode UpdatedNode;
    }
}
