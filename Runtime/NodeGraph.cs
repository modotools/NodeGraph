﻿using System;
using System.Collections.Generic;
using Core.Unity.Interface;
using UnityEngine;
using nodegraph.Data;
using static nodegraph.Operations.NodeGraphOperations;

namespace nodegraph
{
    /// <summary> Base class for all node graphs </summary>
    [Serializable]
    public abstract class NodeGraph : ScriptableObject, INodeGraph, IDependencies
    {
        public virtual ref ScriptableObject[] NodeObjs => ref m_nodes;

        #region New Port Handling
        public List<NodePort> NodePorts { get; } = new List<NodePort>();
        public ref Connection[] Connections => ref m_connections;
        public Dictionary<Connection, List<Vector2>> ReroutePoints { get; } = new Dictionary<Connection, List<Vector2>>();
        #endregion

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] protected internal ScriptableObject[] m_nodes = new ScriptableObject[0];
        [SerializeField] protected internal Connection[] m_connections = new Connection[0];
#pragma warning restore 0649 // wrong warnings for SerializeField


        // Remove all nodes prior to graph destruction
        protected virtual void OnDestroy()
        {
            #if UNITY_EDITOR
            this.Editor_Clear();
            #endif
        }

        public virtual IEnumerable<ScriptableObject> Dependencies
            => m_nodes;
    }
}