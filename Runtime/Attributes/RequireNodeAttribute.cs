using System;

namespace nodegraph.Attributes
{
    /// <summary> Automatically ensures the existence of a certain node type, and prevents it from being deleted. </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class RequireNodeAttribute : Attribute
    {
        public readonly Type Type0;
        public readonly Type Type1;
        public readonly Type Type2;

        /// <summary> Automatically ensures the existence of a certain node type, and prevents it from being deleted </summary>
        public RequireNodeAttribute(Type type)
        {
            Type0 = type;
            Type1 = null;
            Type2 = null;
        }

        /// <summary> Automatically ensures the existence of a certain node type, and prevents it from being deleted </summary>
        public RequireNodeAttribute(Type type, Type type2)
        {
            Type0 = type;
            Type1 = type2;
            Type2 = null;
        }

        /// <summary> Automatically ensures the existence of a certain node type, and prevents it from being deleted </summary>
        public RequireNodeAttribute(Type type, Type type2, Type type3)
        {
            Type0 = type;
            Type1 = type2;
            Type2 = type3;
        }

        public bool Requires(Type type)
        {
            if (type == null) return false;
            if (type == Type0) return true;
            if (type == Type1) return true;
            return type == Type2;
        }
    }
}