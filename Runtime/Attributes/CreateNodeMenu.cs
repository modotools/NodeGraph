using System;

namespace nodegraph.Attributes
{
    /// <summary> Manually supply node class with a context menu path </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CreateNodeMenuAttribute : Attribute
    {
        public readonly string MenuName;
        public readonly int Order;
        /// <summary> Manually supply node class with a context menu path </summary>
        /// <param name="menuName"> Path to this node in the context menu. Null or empty hides it. </param>
        public CreateNodeMenuAttribute(string menuName)
        {
            MenuName = menuName;
            Order = 0;
        }

        /// <summary> Manually supply node class with a context menu path </summary>
        /// <param name="menuName"> Path to this node in the context menu. Null or empty hides it. </param>
        /// <param name="order"> The order by which the menu items are displayed. </param>
        public CreateNodeMenuAttribute(string menuName, int order)
        {
            MenuName = menuName;
            Order = order;
        }
    }
}