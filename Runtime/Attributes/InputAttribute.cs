using System;
using nodegraph.Interface;

namespace nodegraph.Attributes
{
    /// <summary> Mark a serializable field as an input port. You can access this through <see cref="Node.GetInputPort(string)"/> </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class InputAttribute : Attribute, IInputOutputAttribute
    {
        public const ConnectionType DefaultConnectionType = ConnectionType.Override;

        public ShowBackingValue BackingValue { get; }
        public ConnectionType ConnectionType { get; }
        public bool DynamicPortList { get; }
        public TypeConstraint TypeConstraint { get; }

        /// <summary> Mark a serializable field as an input port. You can access this through <see cref="Node.GetInputPort(string)"/> </summary>
        /// <param name="backingValue">Should we display the backing value for this port as an editor field? </param>
        /// <param name="connectionType">Should we allow multiple connections? </param>
        /// <param name="typeConstraint">Constrains which input connections can be made to this port </param>
        /// <param name="dynamicPortList">If true, will display a reorderable list of inputs instead of a single port. Will automatically add and display values for lists and arrays </param>
        public InputAttribute(ConnectionType connectionType = DefaultConnectionType, ShowBackingValue backingValue = ShowBackingValue.Unconnected, TypeConstraint typeConstraint = TypeConstraint.None, bool dynamicPortList = false)
        {
            BackingValue = backingValue;
            ConnectionType = connectionType;
            DynamicPortList = dynamicPortList;
            TypeConstraint = typeConstraint;
        }
    }
}