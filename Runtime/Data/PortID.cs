using System;
using UnityEngine;
using nodegraph.Operations;

namespace nodegraph.Data
{
    [Serializable]
    public struct PortID
    {
        public ScriptableObject Node;
        public string FieldName;

        public static bool operator ==(PortID a, PortID b) => a.Node == b.Node && string.Equals(a.FieldName, b.FieldName);
        public static bool operator !=(PortID a, PortID b) => !(a == b);

        public override string ToString() => Node == null ? $"[NULL].{FieldName}" : $"[{Node.name}].{FieldName}";
    }
}