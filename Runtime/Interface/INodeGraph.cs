using System.Collections.Generic;
using Core.Unity.Interface;
using UnityEngine;
using nodegraph.Data;

namespace nodegraph
{
    public interface INodeGraph : IScriptableObject
    {
        ref ScriptableObject[] NodeObjs { get; }

        #region New Port Handling
        List<NodePort> NodePorts { get; }
        ref Connection[] Connections { get; }
        Dictionary<Connection, List<Vector2>> ReroutePoints { get; }
        #endregion
    }
}