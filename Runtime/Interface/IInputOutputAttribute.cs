﻿namespace nodegraph.Interface
{
    public interface IInputOutputAttribute
    {
        ShowBackingValue BackingValue { get; }
        ConnectionType ConnectionType { get; }
        bool DynamicPortList { get; }
        TypeConstraint TypeConstraint { get; }
    }
}