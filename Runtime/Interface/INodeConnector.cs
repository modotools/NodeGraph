﻿using System;

namespace nodegraph.Interface
{
    public interface INodeConnector
    {
    }
    public interface INodeConnector<T> : INodeConnector
    {
    }

    public static class NodeConnectorOperations
    {
        public static Type GetType<T>(this INodeConnector<T> connector) => typeof(T);
        public static bool CanConnect<T>(this INodeConnector<T> connector, INode node) => node.NodeData is T;
    }
}