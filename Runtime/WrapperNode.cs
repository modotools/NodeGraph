﻿using System;
using Core.Events;
using Core.Unity.Extensions;
using UnityEngine;
using nodegraph.Data;
using static nodegraph.Operations.NodeOperations;

namespace nodegraph
{
    [Serializable]
    public class WrapperNode : Node
    {
        public override INodeData NodeData => (INodeData) m_data;
        public override string Name
        {
            get
            {
                var dataName = NodeData?.Name;
                if (dataName == null)
                    return "";
                if (!string.Equals(dataName, name))
                    name = dataName;

                return dataName;
            }
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ScriptableObject m_data;
#pragma warning restore 0649

#if UNITY_EDITOR
        public void Editor_SetNodeData(INodeData data)
        {
            name = data.name;
            m_data = (ScriptableObject) data;
            EventMessenger.TriggerEvent(new UpdatePorts(){ Node = this });
        }
#endif

        /// <summary> Returns a value based on requested port output. Should be overridden in all derived nodes with outputs. </summary>
        /// <param name="port">The requested port.</param>
        public override object GetOutputValue(PortID port)
        {
            Debug.LogWarning("No GetValue(NodePort port) override defined for " + GetType());
            return null;
        }

        void OnDestroy()
        {
            if (m_data != null)
                m_data.DestroyEx();
        }
    }
}
