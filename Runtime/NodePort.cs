﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Operations;

namespace nodegraph
{
    [Serializable]
    public class NodePort
    {
        #region Properties
        public PortID ID => new PortID() { FieldName = m_fieldName, Node = m_node };
        public int ConnectionCount => Connections.Count();

        /// <summary> Return the first non-null connection </summary>
        // foreach (var c in connections) if (c!= null) return c.Port;
        public PortID ConnectedPortID
        {
            get
            {
                foreach (var c in Connections)
                {
                    if (c.To == ID)
                        return c.From;
                    if (c.From == ID)
                        return c.To;
                }

                return default;
            }
        }

        public Connection ConnectionFrom => Graph.GetConnectionFrom(ID);
        public Connection ConnectionTo => Graph.GetConnectionTo(ID);
        public PortID ConnectedToPortID => ConnectionFrom.To;
        public PortID ConnectedFromPortID => ConnectionTo.From;

        public IO Direction
        {
            get => m_direction;
            set => m_direction = value;
        }
        public ConnectionType ConnectionType
        {
            get => m_connectionType;
            set => m_connectionType = value;
        }
        public TypeConstraint TypeConstraint
        {
            get => m_typeConstraint;
            set => m_typeConstraint = value;
        }

        /// <summary> Is this port connected to anything? </summary>
        public bool IsConnected => m_graph != null && Connections.Any();

        public bool IsInput => Direction == IO.Input;
        public bool IsOutput => Direction == IO.Output;

        public string FieldName
        {
            get => m_fieldName;
            set => m_fieldName = value;
        }

        public INode Node => (INode) m_node;
        public bool IsDynamic => m_dynamic;
        public bool IsStatic => !m_dynamic;

        public Type ValueType
        {
            get
            {
                if (m_valueType == null && !string.IsNullOrEmpty(m_typeQualifiedName))
                    m_valueType = Type.GetType(m_typeQualifiedName, false);
                return m_valueType;
            }
            set
            {
                m_valueType = value;
                if (value != null) 
                    m_typeQualifiedName = value.AssemblyQualifiedName;
            }
        }
        Type m_valueType;

        public IEnumerable<Connection> Connections => Graph.GetConnectionsFor(ID);
        #endregion

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] string m_fieldName;
        [SerializeField] ScriptableObject m_node;
        [SerializeField] string m_typeQualifiedName;
        //[SerializeField] List<Connection> m_connections = new List<Connection>();
        [SerializeField] IO m_direction;
        [SerializeField] ConnectionType m_connectionType;
        [SerializeField] TypeConstraint m_typeConstraint;
        [SerializeField] bool m_dynamic;
        [SerializeField] ScriptableObject m_graph;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public INodeGraph Graph => (INodeGraph) m_graph;

        #region Constructors
        /// <summary> Construct a static targetless nodeport. Used as a template. </summary>
        public NodePort(FieldInfo fieldInfo)
        {
            m_fieldName = fieldInfo.Name;
            ValueType = fieldInfo.FieldType;
            m_dynamic = false;
            var attributes = fieldInfo.GetCustomAttributes(false);
            foreach (var a in attributes)
            {
                switch (a)
                {
                    case InputAttribute ia:
                        m_direction = IO.Input;
                        m_connectionType = ia.ConnectionType;
                        m_typeConstraint = ia.TypeConstraint;
                        break;
                    case OutputAttribute oa:
                        m_direction = IO.Output;
                        m_connectionType = oa.ConnectionType;
                        m_typeConstraint = oa.TypeConstraint;
                        break;
                }
            }
        }

        /// <summary> Copy a nodePort but assign it to another node. </summary>
        public NodePort(NodePort nodePort, INode node)
        {
            m_fieldName = nodePort.m_fieldName;
            ValueType = nodePort.m_valueType;
            m_direction = nodePort.Direction;
            m_dynamic = nodePort.m_dynamic;
            m_connectionType = nodePort.m_connectionType;
            m_typeConstraint = nodePort.m_typeConstraint;
            m_node = (ScriptableObject)node;
            //Debug.Log($"New NodePort ID = {ID} Graph = {node.Graph}");

            m_graph = (ScriptableObject) node.Graph;
        }

        /// <summary> Construct a dynamic port. For runtime-created ports. </summary>
        public NodePort(string fieldName, Type type, IO direction, ConnectionType connectionType, TypeConstraint typeConstraint, INode node)
        {
            m_fieldName = fieldName;
            ValueType = type;
            m_direction = direction;
            m_node = node as ScriptableObject;
            //Debug.Log($"New dynamic NodePort ID = {ID} Graph = {node.Graph}");

            m_graph = node?.Graph as ScriptableObject;
            m_dynamic = true;
            m_connectionType = connectionType;
            m_typeConstraint = typeConstraint;
        }
        #endregion

       
        public override string ToString() => $"{ID.ToString()} ({GetHashCode()})";
    }
}


///// <summary> Checks all connections for invalid references, and removes them. </summary>
    //public void VerifyConnections()
    //{
    //    for (var i = m_connections.Count - 1; i >= 0; i--)
    //    {
    //        if (m_connections[i].Node!= null &&
    //            !string.IsNullOrEmpty(m_connections[i].FieldName) &&
    //            m_connections[i].Node.GetPort(m_connections[i].FieldName)_!=null)
    //            continue;
    //        m_connections.RemoveAt(i);
    //    }
    //}
    ///// <summary> Return the output value of this node through its parent nodes GetValue override method. </summary>
    ///// <returns> <see cref="nodegraph.Node.GetOutputValue"/> </returns>
    //public object GetOutputValue()
    ////    => Direction == IO.Input? null : Node.GetOutputValue(ID);
    ///// <summary> Return the output value of the first connected port. Returns null if none found or invalid.</summary>
    ///// <returns> <see cref="NodePort.GetOutputValue"/> </returns>
    //public object GetInputValue() => Direction == IO.Output? null 
    //    : Connections.First().From.GetNodePort().GetOutputValue();
    ///// <summary> Return the output values of all connected ports. </summary>
    ///// <returns> <see cref="NodePort.GetOutputValue"/> </returns>
    //public object[] GetInputValues()
    //{
    //    if (Direction == IO.Output)
    //        return null;
    //    var objs = new object[ConnectionCount];
    //    for (var i = 0; i < ConnectionCount; i++)
    //    {
    //        var connectedPort = m_connections[i].From.GetNodePort();
    //        if (connectedPort == null)
    //        { // if we happen to find a null port, remove it and look again
    //            m_connections.RemoveAt(i);
    //            i--;
    //            continue;
    //        }
    //        objs[i] = connectedPort.GetOutputValue();
    //    }
    //    return objs;
    //}
    ///// <summary> Return the output value of the first connected port. Returns null if none found or invalid. </summary>
    ///// <returns> <see cref="NodePort.GetOutputValue"/> </returns>
    //public T GetInputValue<T>() 
    //    => GetInputValue() is T typedObj? typedObj : default;
    ///// <summary> Return the output values of all connected ports. </summary>
    ///// <returns> <see cref="NodePort.GetOutputValue"/> </returns>
    //public T[] GetInputValues<T>()
    //{
    //    var objs = GetInputValues();
    //    var ts = new T[objs.Length];
    //    for (var i = 0; i < objs.Length; i++)
    //    {
    //        if (objs[i] is T) 
    //            ts[i] = (T)objs[i];
    //    }
    //    return ts;
    //}
    ///// <summary> Return true if port is connected and has a valid input. </summary>
    ///// <returns> <see cref="NodePort.GetOutputValue"/> </returns>
    //public bool TryGetInputValue<T>(out T value)
    //{
    //    var obj = GetInputValue();
    //    if (obj is T typedObj)
    //    {
    //        value = typedObj;
    //        return true;
    //    }
    //    value = default;
    //    return false;
    //}
    ///// <summary> Return the sum of all inputs. </summary>
    ///// <returns> <see cref="NodePort.GetOutputValue"/> </returns>
    //public float GetInputSum(float fallback)
    //{
    //    var objs = GetInputValues();
    //    return objs.Length == 0? fallback 
    //        : objs.OfType<float>().Sum();
    //}
    ///// <summary> Return the sum of all inputs. </summary>
    ///// <returns> <see cref="NodePort.GetOutputValue"/> </returns>
    //public int GetInputSum(int fallback)
    //{
    //    var objs = GetInputValues();
    //    return objs.Length == 0? fallback 
    //        : objs.OfType<int>().Sum();
    //}